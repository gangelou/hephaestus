import os
import pandas as pd
from osccode_api import get_seismic
import numpy as np
import math
from scipy.stats import norm
from scipy.stats import multivariate_normal


class load_freqs(object):
   def __init__(self, ffile):
      self.fmt=1
      try:
        freq_data=np.genfromtxt(ffile, delimiter='', dtype=[int,int,float, float], names=True) 
      except: freq_data=np.genfromtxt(ffile, delimiter=',', dtype=[int,int,float, float], names=True)
      
      dnames={'nu':'v', 'dnu':'err'}
      nnames= freq_data.dtype.names
      names=[ dnames[x] if x in dnames else x for x in nnames ]
      freq_data.dtype.names=names
      self.modes=freq_data

   def load_seismic_obs(self, modes, numax):
       """Get separations, ratios etc"""

       ### Lets work out what degrees we are working with
       lk2=['Dnu0', 'dnu02', 'dnu13', 'r02','r01','r13','r10']
       fcols=[x for x in lk2 if '0' in x or '1' in x]    
       if 3 not in modes['l']:
           fcols=[x for x in fcols if '3' not in x]
       if 2 not in modes['l']:
           fcols=[x for x in fcols if '2' not in x]         
       if 1 not in modes['l']:
           fcols=[x for x in fcols if '1' not in x] 
       
       radial_order=False
       ### This is a pass through once to work out which columns we need to calculate.
       ### Pertains to the case when we want individual ratios
       if radial_order == True:
           rdict=get_freqs(fcols, modes, numax['value'], get_rs=True) 
           for key, olist in rdict.items():
               for nord in olist:
                  par= key+'_'+str(nord)
                  fcols.append(par)


       
       ### Not sure how we should store it. List and dictionary for now
       self.fcols=fcols
       self.inst=get_seismic(fcols, modes, numax['value'])
       #self.freq_consts={k:v for k,v in zip(self.fcols,self.inst)}
       
       ### Should probably work out covariance matrix for seismic data
       instantions=1000
       nmxs= np.random.normal(numax['value'],numax['uncertainty'],instantions)
       results2= list(map(self.rand_instf,nmxs)) 
       self.corr_matrix=np.corrcoef(np.asarray(results2).T)
       self.cov_matrix=np.cov(np.asarray(results2).T)
       
       ### Define some errors for our seismic constraints
       self.ferrs={}
       self.freq_consts={}
       for i, col in enumerate(self.fcols):
           self.freq_consts[col]=np.asarray(results2)[:,i].mean()
           self.ferrs[col+'_ERR']=np.asarray(results2)[:,i].std()


       
   def rand_instf(self,nmx): 
     ### Being a bit naughty and leaving some checks out. 

     inst=np.array([-1]*len(self.fcols))
     lfd=len(self.modes['l'])
     fd2=np.full((lfd),-1, dtype=[('l','int'),('n','int'),('v','float')])
     fd2['l']=self.modes['l']
     fd2['n']=self.modes['n']
     np.random.seed()
     fd2['v']=[np.random.normal(self.modes['v'][i],self.modes['err'][i]) for i in range(lfd)]
     inst=get_seismic(self.fcols, np.asarray(fd2), nmx)
     return inst  


class obs_star(object): 
    def __init__(self):
        
        ### Basic Loading of obsdata
        self.name='sun'
        self.freqfile='/afs/mpa/project/ml2/Hephaestus/data/Sun/sun-freqs.dat'
        self.obsfile='/afs/mpa/project/ml2/Hephaestus/data/Sun/sun-obs.dat'
        xcols=[]
        obs_data= pd.read_table(self.obsfile,delim_whitespace=True)
        obs_data.columns=[x.lower() for x in obs_data.columns]    
        xcols+=list(obs_data['name']) 
        self.n_obs=len([x for x in xcols if x!='nu_max'])
        
        self.obsdict={}
        self.obserr={}
        for i,col in enumerate(xcols):
           self.obsdict[col]=obs_data['value'][i]
           self.obserr[col+'_ERR']= obs_data['uncertainty'][i]
        
        print('self.name',self.name)
        print('self.freqfile',self.freqfile)
        print('self.obsfile',self.obsfile)
        print('obs_data', obs_data)
        print('xcols', xcols)
        print('self.n_obs',self.n_obs)
        print('self.obsdict',self.obsdict)
        print('self.obserr',self.obserr)
        
        
        ### Frequency Stuff
        self.freqs=load_freqs(self.freqfile)
        ### Get separations and ratios if we dont already  
        if self.freqs.fmt in [1,2]:
            ix=xcols.index('nu_max')         
            self.freqs.load_seismic_obs(self.freqs.modes,obs_data.iloc[ix]) 
            self.n_obs += len(self.freqs.cov_matrix)
         
         
        print('self.freqs.modes',self.freqs.modes)
        print('self.freqs.fcols', self.freqs.fcols)
        print('self.freqs.inst', self.freqs.inst)
        print('self.freqs.cov_matrix',self.freqs.cov_matrix)
        print('self.freqs.ferrs', self.freqs.ferrs)
        print('self.freqs.freq_consts', self.freqs.freq_consts)
         
        ### Combine -obs and -freq parameters 
        self.obsdict = {**self.obsdict, **self.freqs.freq_consts}
        self.obserr  = {** self.obserr, **self.freqs.ferrs}        
        
        self.obs=obs_data
        self.xcols=xcols
        
        print('self.obsdict',self.obsdict)
        print('self.obserr',self.obserr)
        print('self.obs',self.obs)
        print('self.xcols',self.xcols)


class cmodels (object):
    def __init__(self,name,a):
         self.name=name
         self.obsdata=a
         dtypes=[('nmod', int), ('chi_ev',float), ('chi_osc', float), ('chi_total', float),
                ('mle_ev',float), ('mle_osc', float),('mle_total', float)]
         dtypes+=[(x,float) for x in self.obsdata.obsdict.keys()]
         self.model_observables=np.full(1, np.nan, dtype=dtypes)
         
         for root, dirs, files in os.walk(name):
             for f in files:
                 if f.endswith('.ascii'):
                     self.freqf=os.path.join(root,f)
                 if f.endswith(os.path.basename(self.name)+'.log'):
                     self.logf=os.path.join(root,f)
                     
         self.w=self.readlog(self.logf)
         self.model_observables=self.get_structure_obs(self.logf,
                                                        self.w,
                                                        self.model_observables,
                                                        self.obsdata,
                                                        1)
         self.model_observables=self.get_osc_obs(self.logf,
                                                      self.w,
                                                      self.model_observables,
                                                      self.obsdata,
                                                      [self.model_observables['nmod']], 
                                                      [], 1)
         #print(self.model_observables, os.path.basename(self.name))
         
         
         
         
         
    def readlog(self, fname):         
        return np.genfromtxt(
            fname,
            comments='#',
            delimiter=[7,16,7, 3,6,7,9,8,6,7,7, 6,7, 5,8,7,7,7,5, 4,8,8,8,4])


    def get_Teff(self, fname=None, arr=None):
        """Method to extract temp from log file"""
        if arr is not None:
            w = arr
        else:
            w = self.readlog(fname) 
        return [10**w[:, 10][i] for i in range(len(w))]   
    
    
    def get_mass(self, fname=None, arr=None):
        """Method to return star mass from garstec output"""
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname)           
        return w[:, 4]

    def get_lum(self, fname=None, arr=None):
        """Method to return luminosity from garstec output"""
        if arr is not None:
            w=arr
        else:
           w = self.readlog(fname)
        return [10**w[:, 6][i] for i in range(len(w))]
         
    def get_mods(self, fname=None, arr=None):
        """Return list of models from garstec logfile"""
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname) 
        return w[:, 0]
    
    def get_nu_max(self,NMsun=3090, TeffSun=5777, fname=None, arr=None):
        """Method to calculate Log G from garstec output"""
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname) 
        
        nmx = [(NMsun * w[:, 4][i] * ((10.0**w[:, 10][i]) / TeffSun)
                ** 3.5) / 10.0**w[:, 6][i] for i in range(len(w))]
        return nmx    
    
    def get_FeH(self, zx=.01828, fname=None):
        """Method to extraxt [Fe/H] from Garstec Output """
        w = self.readlog(fname)
            
        ### Hacky and quick. No Alpha enhancement. Fix
        log3 = fname + '3'
        w3 = np.genfromtxt(
            log3, comments='#', delimiter=[
                7, 8, 8, 5, 4, 3, 9, 9])
        w3 = np.insert(w3, 0, w3[0], axis=0)
        wz = [1 - w3[:, 7][kk] - w3[:, 6][kk] for kk in range(len(w))]
        feh = [math.log10(wz[kk] / w3[:, 6][kk]) - math.log10(zx)
               for kk in range(len(w))]
        return feh
 
    def get_DNU(self, fname=None, arr=None):
        """Here is where the DNU stuff should go"""
        FeH  = self.get_FeH(fname=fname)
        Teff = self.get_Teff(fname=fname, arr=arr)
        Mass = self.get_mass(fname=fname, arr=arr)
        Lum  = self.get_lum(fname=fname, arr=arr)

        """ 
        print('FeH', len(FeH))
        print('Teff', len(Teff))
        print('Mass', len(Mass))
        print('Lum', len(Lum))"""

        """Scaling Relation Correction from Elisabeth Guggenberger.
            Delta nu = ( (M/Msun)^0.5 * (Teff/Teffsun)^3 *Deltanu_Sun)/ (L/Lsun)^0.75)
            There is also the power law form from nu max dnu=[0.263*i**0.772 for i in nmx]
            (param[0]*feh+param[1])*exp^((param[2]*feh+param[3])*Teff/10000.)*(cos(param[4]*Teff/10000.+(param[5]*feh^2+param[6]*feh+param[7])))
            +(param[8]+param[9]*feh)"""

        func_coeffs = [0.55757590, 1.1315471, -0.94266469, 1.9988931,
                       21.135114, 0.17652742, 0.42097644, 0.75068501, 134.92490, 0.69975320]
        reference = [func_coeffs[0] *
                     FeH[i] +
                     func_coeffs[1] *
                     math.exp((func_coeffs[2] *
                               FeH[i] +
                               func_coeffs[3]) *
                              Teff[i] /
                              10000.) *
                     (math.cos(func_coeffs[4] *
                               Teff[i] /
                               10000. +
                               (func_coeffs[5] *
                                FeH[i]**2 +
                                func_coeffs[6] *
                                FeH[i] +
                                func_coeffs[7]))) +
                     (func_coeffs[8] +
                      func_coeffs[9] *
                      FeH[i]) for i in range(len(Teff))]

        DNU = [(Mass[i]**0.5 * (Teff[i] / 5777)**3 *
                reference[i]) / Lum[i] for i in range(len(Teff))]
        return DNU 
 
 
 
 
    def eval_metrics(self, arr, obsdata,osc_fail=None):
        """Here we calculate our metrics based on 
           the availible information passed in the array."""
        
        larr=len(arr)
        chi_ev=[0]*larr
        mle_ev=[1]*larr
        chi_osc=[0]*larr
        mle_osc=[1]*larr
        
        chidict={}
        freqdic={}
        ### If the total array is np.nan this is our first time through
        if np.isnan(arr['chi_total'][0]):
           eval_cols=[x for x in arr.dtype.names if not np.isnan(np.atleast_1d(arr)[x][0])
                   and x in obsdata.obsdict.keys()]    
           
           ### Save for failed oscillation calculations. lol actally dont need it
           self.levcols=len(eval_cols)
           
           for col in eval_cols:
              chi_parm=[self.chi_sq(obsdata.obsdict[col],
                                    obsdata.obserr[col+'_ERR'],
                                    arr[col][i]) for i in range(larr)]   
        
              mle_parm = [self.weight_MLE(obsdata.obsdict[col],
                                    obsdata.obserr[col+'_ERR'],
                                    arr[col][i]) for i in range(larr)]   
                            
              if col =='Dnu0':
                  ### Put this in the array total column, not the temporary ev column
                  arr['chi_total']=chi_parm
                  arr['mle_total']=mle_parm
              else:
                 ### Keep including in the temporary ev column 
                 chi_ev=[x+y for x,y in zip(chi_ev, chi_parm)]
                 mle_ev=[x*y for x,y in zip(mle_ev, mle_parm)]
           
              
              chidict[col]=[obsdata.obsdict[col],obsdata.obserr[col+'_ERR'],arr[col][0], chi_parm, mle_parm]
           
           self.chidict=chidict
           ### Out of the loop. Set the temporary ev column in the array
           arr['chi_ev']=chi_ev
           arr['mle_ev']=mle_ev
           
           if 'Dnu0' in eval_cols:
               ### Dnu0 component already in the array total column 
               arr['mle_total']=[x*y for x,y in zip(arr['mle_total'], arr['mle_ev'])]
               arr['chi_total']=[x+y for x,y in zip(arr['chi_total'], arr['chi_ev'])] 
                                                                                                    
               if obsdata.freqfile:
                 ### Dnu0 not counted in the ev params normalization  
                 ### But we will count it in the array total chi which 
                 ### we overwrite later once we have seismic info
                 arr['chi_ev']=arr['chi_ev']/(self.levcols-1)   
                 arr['chi_total']=arr['chi_total']/self.levcols

               else:
                  ### No frequencies will be calculated later. 
                  ### DNU is considered a structure constraint so normalise and reset total chi 
                  arr['chi_total']=arr['chi_total']/self.levcols
                  arr['chi_ev']=arr['chi_total']    
               
           else:
               ### Easy case. No Dnu to deal with. Simply normalise and store 
               arr['mle_total']=arr['mle_ev']
               arr['chi_ev']=arr['chi_ev']/self.levcols
               arr['chi_total']=arr['chi_ev']
                        
        
        else:
            ### We now have oscillation data we want to evaluate.
            ### Asteroseismic observables are highly correlated. We need to use to covairance matrix
            ### See http://adsabs.harvard.edu/abs/2014A%26A...569A..21L or Press (1992)
            
            ### Have to only evaluate those we have successfully caclulated freqs for
            for i, row in enumerate(arr):
                if row['nmod'] in osc_fail:
                    ###Essentially now consider Dnu an Ev observable. Already normalised 
                    arr['chi_ev'][i]=arr['chi_total'][i]
                    arr['mle_ev'][i]=arr['mle_total'][i]
                    
                else:
                  data= [row[col] for col in obsdata.freqs.fcols] 
                  
                  arr['chi_osc'][i]= self.Cov_Chi(obsdata.freqs.inst,
                                                  obsdata.freqs.cov_matrix, 
                                                  data)
                  
                  arr['mle_osc'][i]=  self.Cov_MLE(obsdata.freqs.inst,
                                                  obsdata.freqs.cov_matrix, 
                                                  data)
            
                  arr['chi_total'][i]=arr['chi_osc'][i]+arr['chi_ev'][i]
                  arr['mle_total'][i]=arr['mle_osc'][i]+arr['mle_ev'][i]
            
            ### Without the covs     
            for col in self.obsdata.freqs.fcols:
              chi_parm=[self.chi_sq(obsdata.obsdict[col],
                                    obsdata.obserr[col+'_ERR'],
                                    arr[col][i]) for i in range(larr)]     


              mle_parm = [self.weight_MLE(obsdata.obsdict[col],
                                    obsdata.obserr[col+'_ERR'],
                                    arr[col][i]) for i in range(larr)]  

              freqdic[col]=[obsdata.obsdict[col],obsdata.obserr[col+'_ERR'],arr[col][0], chi_parm, mle_parm]             
        self.freqdic=freqdic

        self.ichi=0
        self.imle=1         
        for key, items in freqdic.items():
             self.ichi += items[-2][0]
             self.imle *= items[-1][0]  
        return arr    



    def get_structure_obs(self,logf, logarr, model_observables, obsdata, imcmc=1):
        """Here we get the observables that can be calculated from the structure code"""
        get_evar={'Fe_H':self.get_FeH(fname=logf),
                  'Teff': self.get_Teff(arr=logarr),
                  'M': self.get_mass(arr=logarr),
                  'L': self.get_lum(arr=logarr),
                  'nmod': self.get_mods(arr=logarr),
                  'Dnu0':self.get_DNU(fname=logf,arr=logarr) }
        
        if imcmc==1:
           ### MCMC we just take the last model which is the age we want 
           for key, item in get_evar.items():
              if key in model_observables.dtype.names: 
                 model_observables[key]=item[-1]  
              
           model_observables=self.eval_metrics(model_observables,obsdata)
           
        else:
            ### Need to create a new big array the length of nmod
            big=np.full( len(nmods), np.nan, dtype=model_observables.dtypes)
            for key, item in get_evar.items():
              if key in model_observables.dtype.names:   
                for i in range(len(nmods)):               
                   big[key][i]=item[i]            
            
            ### Evalutate and copy the smallest over 
            big=self.eval_metrics(big,obsdata)
            big=np.sort(big, order='MLE_total')
            model_observables=big[-1* self.parms.max_mods_osc:]
            
        return model_observables   

       
    def chi_sq(self, obs, unc, mod):
        return (obs - mod)**2 / unc**2

    def weight_MLE(self, obs, unc, mod):
        return -(norm.logpdf(mod, loc=obs, scale=unc))

    def Cov_MLE(self, means, covs, data):
        """data are from models, means are from obs in our case"""
        var = multivariate_normal(mean=means, cov=covs)
        nll = var.logpdf(data)
        # print(nll)
        return -nll

    def Cov_Chi(self, means, covs, data):
        """data are from models, means are from obs in our case"""

        ### Verbose coding for clarity
        dif = [x - y for x, y, in zip(data, means)]
        a=np.matrix(covs).I
        b=np.atleast_1d(dif).T
        c=np.atleast_1d(dif) 
     
        chi=np.dot(b,a)
        chi=np.dot(chi,c)
        
        #chi = np.a(dif).T dot np.matrix(covs).I dot dif
        return chi[0]
    
    def get_osc_obs(self, logf, logarr, model_observables,obsdata, osc_pass, osc_fail,imcmc):
        """Add in the contribution from the seismic constraints"""
        
        models=[int(x) for x in model_observables['nmod']]
        
        ### Need the nu-max of each model 
        nmxs=self.get_nu_max(arr=logarr)
        nmods=list(self.get_mods(arr=logarr))
        nmxs=[nmxs[nmods.index(mod)] for mod in models] 
        
        
        for mod in osc_pass:
          idx=models.index(mod)
          freqs = self.get_freqs(mod)
          seismic_vars=get_seismic(obsdata.freqs.fcols, freqs, nmxs[idx])
          for i, col in enumerate(obsdata.freqs.fcols):
             model_observables[col][idx]=seismic_vars[i]                               

        ### Evaluate the metrics
        model_observables= self.eval_metrics(model_observables,obsdata,osc_fail)
       
        ### I dont have a catch all here if something goes wrong with the frequencies coming back
        ### Also should consider a report
       
        return model_observables 

    def get_freqs(self, folder):
        modname = str(folder[0]).zfill(7)
        fname = self.name+'/AMDL/' + modname + '/' + modname + '.ascii'
        return np.genfromtxt(
            fname, usecols=[0, 1, 2, 3], 
              dtype=[('l', int), ('n', int), ('v', float), ('mi', float)])   
   
### ---- Start Here -----         

a=obs_star()  
dirs='/afs/mpa/project/ml2/Hephaestus/run/Sun2/tracks/Sun/MCMC/Iter20/'
models=[os.path.join(dirs,x) for x in os.listdir(dirs)]
mods=[]

for i in models:
    mods.append(cmodels(i,a))
                
ms=sorted(mods, key=lambda x: x.model_observables['chi_total'])
for i in range(5):
  print(ms[i].name)
  print(ms[i].model_observables)
  print(ms[i].chidict)
  print(ms[i].freqdic)
  print('Osc Chi comparison', ms[i].model_observables['chi_osc'], ms[i].ichi)
  print('Osc MLE comparison', ms[i].model_observables['mle_osc'], ms[i].imle)
