import numpy as np


### Load the chain file
cfile='/afs/mpa/project/ml2/Hephaestus/run/Sun4/tracks/Sun/logsMCMC/sun_chain.npz'
a=np.load(cfile)
chains=a['chain']    
ndim=3

### redimensionise to samples. 50 here is the burn in we want to leave out
burnin=30
samples = chains[:, burnin:, :].reshape((-1, ndim))

m_mcmc, b_mcmc, f_mcmc = map(lambda v: (v[1], v[2]-v[1], v[1]-v[0]),
                             zip(*np.percentile(samples, [16, 50, 84],
                                                axis=0)))
                             

print('mass', m_mcmc)
print('X_init',b_mcmc)   
print('age',f_mcmc)                         
