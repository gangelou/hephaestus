#! /opt/anaconda-3/anaconda3/bin/python3
#/home/gangelou/anaconda3/bin/python

import argparse
import datetime
import os
import pandas as pd
import shutil
import sys 
import time


import src.param_read as spr
import src.ggutils as sut
import src.evcode_api as evapi
from src.load_freq import load_freqs
from src.ggmcmc import mcmcStar

class obs_star(object): 
    def __init__(self,name,root,star,parm):
       self.name=name
       self.root=root
       self.obsfile=star

       self.freqfile=self.get_freqfile(root, parm )
       self.startfile=self.get_startf(root,parm )
       print('obstar startfile', self.startfile)
       ### Now we have the files lets load in the data  
       xcols=[]
       obs_data= pd.read_table(self.obsfile,delim_whitespace=True)
       obs_data.columns=[x.lower() for x in obs_data.columns]    
       xcols+=list(obs_data['name']) 
       self.n_obs=len([x for x in xcols if x!='nu_max'])
       
       ### Afterthought. Lets store in a dictionary. Probably useful
       self.obsdict={}
       self.obserr={}
       for i,col in enumerate(xcols):
           self.obsdict[col]=obs_data['value'][i]
           self.obserr[col+'_ERR']= obs_data['uncertainty'][i]
       if 'nu_max' not in xcols and self.freqfile:
          print('Sorry. We are not doing any seismic calculations without numax')
          sut.writer(parm.logf, 'obsstar', 'ERR ', 'Attemptin seismology without numax input')                
          sys.exit(2)

       if self.freqfile:
         self.freqs=load_freqs(self.freqfile,parm)
         
         ### Get separations and ratios if we dont already  
         if self.freqs.fmt in [1,2]:
            ix=xcols.index('nu_max')         
            self.freqs.load_seismic_obs(self.freqs.modes,obs_data.iloc[ix],parm) 
            self.n_obs += len(self.freqs.cov_matrix)
         
         ### Combine -obs and -freq parameters 
         self.obsdict = {**self.obsdict, **self.freqs.freq_consts}
         self.obserr  = {** self.obserr, **self.freqs.ferrs}
         
       print('By my rekoning we have', self.n_obs, 'observable constraints that may or may not be independent')
       sut.writer(parm.logf, 'obsstar', 'INFO', 'I estimate ' + str(self.n_obs) +' observable constraints')     
       self.obs=obs_data
       self.xcols=xcols
              
    def get_freqfile(self,root, parm):
       """Search for the frequency file with the extension we specified"""
       pat=parm.freq_pattern 
       ffile=''
       ffiles=[os.path.join(root,x) for x in os.listdir(root) if x.endswith(pat)]
       
       if len(ffiles) ==0:        
         print('No freq file found. I assume you know want you are doing')
         sut.writer(parm.logf, 'obsstar', 'WARN', 'freq_pattern not mathed in star obsdata dir')         
       elif len(ffiles) > 1:
         print('Multiple freq files found. Not adventerous enough to guess. Pease fix')
         sut.writer(parm.logf, 'obsstar', 'ERR ', 'multiple files match freq_pattern in star obsdata dir -- Stopping')  
         sys.exit(2) 
       else:
         ffile=ffiles[0] 
         print('Taking', ffile, 'as freq file')
         sut.writer(parm.logf, 'obsstar', 'INFO', 'Taking '+ffile +' as freqfile' ) 
       return ffile

    def get_startf(self,root,parm):
       """Search for a potential starting parameters for the search based on the specified extension"""
       pat=parm.starting_pattern
       pfile=''
       pfiles=[os.path.join(root,x) for x in os.listdir(root) if x.endswith(pat)]
       
       if len(pfiles) ==0:        
         print('No starting file found. Will determine search parameters')
         sut.writer(parm.logf, 'obsstar', 'INFO', 'No starting file found. Will determine search parameters')         
       elif len(pfiles) > 1:
         print('Multiple starting files found. Not adventerous enough to guess. Pease fix')
         sut.writer(parm.logf, 'obsstar', 'ERR ', 'multiple files match starting_pattern in star obsdata dir -- Stopping')  
         sys.exit(2) 
       else:
         pfile=pfiles[0] 
         print('Taking', pfile, 'as starting file')
         sut.writer(parm.logf, 'obsstar', 'INFO', 'Taking '+pfile +' as starting file' ) 
       return pfile


def process_dir (parms, starfold):
   """Lets get the observational data"""   
   
   roots=[]
   files=[]
   stars=[] 
   
   ### Get the directories and files. 
   stardir=os.path.join(parms.obs_dir, starfold)

   for root, subdir, path in os.walk(stardir):
      if len(path) > 0:
         for p in path:
           if p.endswith(parms.obs_pattern):
              roots.append(root)
              files.append(os.path.basename(p).replace(parms.obs_pattern, '')) 
              stars.append(os.path.join(root,p))

   starc=[obs_star(files[i], roots[i], stars[i], parms) for i in range(len(files))]

   return starc


def main():
    """A skeleton version of the Hephaestus optimization code designed by George Angelou.
      We process each star one at a time with this code unlike the
      needlessly complicated previous versions. Must supply a data directory and a parameter file"""
      
   
    ### Parse args
    parser = argparse.ArgumentParser(description='Hephaestus stellar optimization code')
    parser.add_argument(dest="parentd", type=str,
                    help="Directory Name in $data_dir in which to process")

    parser.add_argument('-p', dest="parmfile", type=str, default="./grid_in.params",
                    help="Location of RF parmaterfile")
    args = parser.parse_args()
    
    
    ### Time Stamp
    ts = time.time()
    stt = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H.%M.%S')

    ### Get the Parms
    parms = spr.parameters(args.parmfile)
    parms.set_dir()
    parms.st = stt
    parms.home = os.getcwd()

    ### Initialise the Log File and copy Parameter File.
    logfile = parms.create_log()
    parms.logf=logfile
    sut.writer(logfile, 'main', 'INFO', 'Grid Generator Initialised', st=stt)
    sut.writer(
        logfile,
        'main',
        'INFO',
        'Grid parameter file successfully read in',
        st=stt)

    shutil.copy(args.parmfile, parms.run_dir + '/admin/' + parms.runname + '.param')  


    ### Safety first 
    parms.check_ranges(parms)
    parms.check_codes(parms)
    parms.check_algorithms(parms)

    ### Evcode paramter file needs some global parameter information 
    evparm = evapi.ev_parmfile(parms.evcode, parms.garsdir, lf=parms.logfile)
    evparm.stop = parms.stop_criteria
    evparm.sv = parms.stop_value
    evparm.evfunc = evapi.evcode(parms)
    parms.zx = evparm.evfunc.getzx()
    sut.writer(logfile, 'main', 'INFO', parms.evcode + ' parameter file read in')

    parms.init_modlist = sut.InitialModels(parms).initmods
    sut.writer(logfile, 'main', 'INFO', 'Read in list of initial models')


    if parms.verbose:
        # Check the Input params
        attrs = vars(parms)
        print(', '.join("%s: %s" % item for item in attrs.items()))
        atts2 = vars(evparm)
        print(', '.join("%s: %s" % item for item in atts2.items()))


    ### Initialization Done. Lets Optimize. 
    stars=process_dir(parms, args.parentd)
    
    print('Detected ', len(stars), ' stars to process')
    print([star.name for star in stars])
    sut.writer(logfile, 'main', 'INFO', 'Detected ' + str(len(stars)) + ' stars to process')
    
    for star in stars:
       print('Processing', star.name)
       sut.writer(logfile, 'main', 'INFO', 'Processing ' + star.name)
       print(star.xcols)
       
       if parms.local_search_algorithm =='MCMC':
          mcmcStar(star,parms,evparm) 
          
       
       #optimize(star,parms,evparm)  
       print('Finished processing', star.name)
       sut.writer(logfile, 'main', 'INFO', 'Finished processing ' + star.name)
    
    sut.writer(logfile, 'main', 'INFO', 'By Some Miracle the Optimizer Finished')
    print('By Some Miracle the Optimizer Finished')
   

if __name__ == "__main__":
    main()
