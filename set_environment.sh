#!/bin/csh

module load intel
module load anaconda3
### Garstec Settings
setenv GPATH /afs/mpa/project/ml2/Hephaestus/codes/Garstec13/GCA_GARS_source_July2018/
setenv PATH ${PATH}:$GPATH/BIN/common
setenv PATH ${PATH}:$GPATH/BIN/x86_64_linux414
setenv F_UFMTENDIAN big

echo IFORT loaded
echo anaconda3 loaded
echo GPATH: $GPATH
echo ENDIAN: $F_UFMTENDIAN


### Adipls Settings
setenv aprgdir /afs/mpa/project/ml2/Hephaestus/codes/adipack.v0_3
setenv PATH ${PATH}:/afs/mpa/project/ml2/Hephaestus/codes/adipack.v0_3/bin

echo aprgdir: $aprgdir
