import sys
from src.ggutils import writer
import src.osccode_api as osapi 
from scipy.stats import norm
import os
import numpy as np
from scipy.stats import multivariate_normal
import random
class structure(object):
    """Compute and analayse the provided parameters
        i,                  -- Which row in the array to process
        arr                 -- Array of model parameters to calculate 
        stardir,            -- Corresponding list of directories where models will be processed
        parms,              -- Hephaestuts parameter file
        evparms,            -- Evolution code parameter file
        obsdata,            -- Obsdata
        freqs,              -- Freqdata
        warnfile,           -- logfile
        output,             -- Array where to store outputs. Think its done this way because of multiprocessing
        af='',              -- Archive file. If you want to store all calculations
        monitor=True,       -- I think this monitors for stopping conditions and kills the code??
        modedict={},        -- dictionary of modes to calculate specifically
        ratiodict={},       -- dictionary of ratios to specifically calculate
        mcmc_parms={})      -- Special settings from the mcmc algorithm
        
        
        
        So we are clear for non-MCMC case:
        We calculate Chi/MLE for EV parms and these are correctly stored in the ev_chi/ev_mle columns
        We calculate Dnu0 from scaling relations to help zero in on the best models along the track we want to follow up on . 
        The Dnu0 scaling contribution is added in the total Chi column. Once we have selected the models we want to calculate frequencies
        for we recalculate DNU and other seismic paramters from the frequencies. Their contribution goes into the osc_chi
        column. We then recalculate chi total from chi_ev and chi_osc which gets rid of the scaling relation contribution.
        
        """
        
        
    def __init__(
            self,
            i,
            arr,
            stardir,
            parms,
            evparms,
            obsdata,
            warnfile,
            output,
            af='',
            monitor=True,
            modedict={},
            ratiodict={},
            mcmc_parms={}):

        self.i = i
        self.arr = arr
        self.obsdata = obsdata
        self.parms = parms
        self.stardir = stardir[0]
        self.warnf = warnfile
        self.evparms = evparms
        self.af = af
        self.osccode = osapi.osccode(parms)
        self.wd = os.getcwd()
        self.modedict = modedict
        self.ratiodict = ratiodict
        self.mcmc_parms = mcmc_parms        
        

        ### We don't search archives or precomputed models when using MCMC algorithm
        if len(self.mcmc_parms) == 0:
            self.imcmc = 0
        else:
            self.imcmc = 1
            compute_star = True
            self.parms.max_mods_osc=1
                           
        ### Lets define some dictionaries and arrays to store our data
        ### Nmod always defined before np.nan checks. So no problems 
        dtypes=[('nmod', int), ('chi_ev',float), ('chi_osc', float), ('chi_total', float),
                ('mle_ev',float), ('mle_osc', float),('mle_total', float)]
        dtypes+=[(x,float) for x in self.obsdata.obsdict.keys()]
        
        self.model_observables=np.full(self.parms.max_mods_osc, np.nan, dtype=dtypes)
       
        ### Testing purposes
        #while True:
        #    self.model_observables['mle_total']=random.random()*10000
        #    self.output=self.model_observables[0]
        #    return 
       
        ### Move into the star direcotry to run things 
        os.chdir(self.stardir)
         
          
        if compute_star:
          ### No search for close models. Just run from the starting models library. 
          
          ### Lets get the specified options we want for running the code. 
          kwargs = self.get_kwargs(stardir[1])
          print(kwargs) 
          ### Create an evolution code parameter file
          ### Run the EV code and return an exit code; 
          ### 1: Successfully finished; 2: Finished without having to stop
          self.evparms.evparm.create_ev_parm(arr, self.warnf, **kwargs) 
          
          ### May have to monitory to stop evolution beyond MS if age stop doesnt go well with mass
          exitcode = evparms.evfunc.runev( self.arr,
                                           self.obsdata,
                                           self.parms,
                                           self.evparms,
                                           self.warnf,
                                           monitor)
          
          ### We are already in the local stardir 
          logf = arr['ID'] + '.log'
          exitcode=self.confirm_EV_exit(exitcode, logf)   

          
          self.logarr=self.evparms.evfunc.readlog(logf)
          if len(self.logarr) < 2:
             ### Probable failed
             self.model_observables['mle_total'][0]=-np.inf
             self.output=self.model_observables[0]
             os.chdir(self.parms.home)
             return
             
          self.model_observables=self.get_structure_obs(logf,
                                                        self.logarr,
                                                        self.model_observables,
                                                        self.obsdata,
                                                        self.imcmc)
          
          
          writer(self.warnf, 'structure','INFO',arr['ID']+' Evolution finished and structure contribution to MLE determined' )

          ### Do we need to run the oscillation code?
          if self.obsdata.freqfile:
              
              osc_mods=[int(x) for x in self.model_observables['nmod']]
              
              self.osccode.oscfunc.make_run_dir()
              osc_pass, osc_fail = self.osccode.runosc(
                        osc_mods, arr['ID'], self.evparms, logf, imcmc=self.imcmc)
              
              if len(osc_fail) > 0:
                 writer(self.warnf, 'structure','WARN','Returned from seismic calculations with ' +str(len(osc_fail)) +' failures',
                        " ".join([str(x) for x in osc_fail]))

              self.model_observables=self.get_osc_obs(logf,
                                                      self.logarr,
                                                      self.model_observables,
                                                      self.obsdata,
                                                      osc_pass, 
                                                      osc_fail,
                                                      self.warnf,
                                                      self.imcmc)
              
              writer(self.warnf, 'structure','INFO',arr['ID']+' Oscillations calculated and seismic contribution to MLE determined' )    
          midx=np.argmin(self.model_observables['mle_total'])
          cidx=np.argmin(self.model_observables['chi_total'])
          
          if midx !=cidx:
              writer(self.warnf, 'structure','WARN',arr['ID']+' MLE and CHI prefer different models',
                     'MLE= '+str(self.model_observables['nmod'][midx]) + ' CHI= '+str(self.model_observables['nmod'][cidx]))
              
          if self.parms.verbose:
            print(self.model_observables)
          
          self.output=self.model_observables[midx]
          os.chdir(self.parms.home)     
        else:
          ### Do a search before deciding to run 
          print('Not yet re-implenented. Need to redesign')
          sys.exit(2)
          
  
    def confirm_EV_exit(self,exitcode, logf):
        """Some checks on the execution of the stellar evolution code"""
        
        ### Exit before the end of the evolution when we dont want that 
        if exitcode == 0 and 'NO' not in self.parms.stop_criteria:
            writer(self.warnf, 'structure','WARN',
                       'I think ' + str(self.arr['ID']) + ' has crashed prematurely',
                       'This will impact your search and weighting')
           
        ### Successfully Evoled far enough. But lets try avoid losing a race condition
            if exitcode == 1:
                exitcode = self.check_evlog(logf) 
  
        return exitcode
    
    def get_kwargs(self, stardir):
        kwargs = {'mod': stardir + '.mod'}
        if self.parms.evcode.lower() == 'gars':
            kwargs['gkeys'] = self.parms.gkeys
            dstop = {}
            
            ### I have Garstec stopping at the appropriate Dnu0. 
            ### Useful for some search algorithms. Need to stop at lower limit.
            if 'Dnu0' in self.obsdata.obsdict.keys():
                dnu=self.obsdata.obsdict['Dnu0']-self.obsdata.obserr['Dnu0_ERR']
                dstop['DDNU'] = dnu
            else: dstop['DDNU'] = -135.0 
            #if 'TEFF' in self.obsdata.dtype.names:
                #dstop['DTEF'] = self.obsdata[parms.catdict['TEFF']][0]
                
            kwargs['OPT'] = dstop
            
            if self.imcmc == 1:
                kwargs['MCMC'] = self.mcmc_parms
                ### We dont care about dropping the timestepping
                dstop['DDNU'] = -135.0 
             
            ### Effmix is in mcmc parms if overshoot parameter != 0 
            ### Add the key to turn it on
            if 'EffMix' in self.mcmc_parms:
               kwargs['gkeys'].append(82) 
        return kwargs   
    
    
    def check_evlog(self, logf):
        """Routine to chek that our logfile has been written to disk"""
        w = []
        try:
           w = self.evparms.evfunc.readlog(logf)
        except:
            pass

        # 1. Avoid race condition. Return error if something is wrong.
        counter = 0
        while len(w) == 0:
            time.sleep(30)
            try:
                w = self.evparms.evfunc.readlog(logf)

            except:
                pass
            if counter == 6:
                writer(
                    self.warnf,
                    'analyse_log',
                    'WARN',
                    'Trying to Calculate Chi Sq for ' +
                    str(
                        self.arr['ID']),
                    "There doesn't appear to be any models here. Waited 6 minutes -- Removing")

                return 0

            counter += 1
            if self.parms.verbose:
                 print(str(self.arr['ID']), counter)
        return 1
 
 
 
    def get_structure_obs(self,logf, logarr, model_observables, obsdata, imcmc):
        """Here we get the observables that can be calculated from the structure code"""
        get_evar={'Fe_H':self.evparms.evfunc.get_FE_H(fname=logf),
                  'Teff': self.evparms.evfunc.get_TEFF(arr=logarr),
                  'M': self.evparms.evfunc.get_MASS(arr=logarr),
                  'L': self.evparms.evfunc.get_LUM(arr=logarr),
                  'nmod': self.evparms.evfunc.get_mods(arr=logarr),
                  'age':self.evparms.evfunc.get_AGE(arr=logarr),
                  'X_c': self.evparms.evfunc.get_Xcentre(arr=logarr),
                  'log_g':self.evparms.evfunc.get_LOG_G(arr=logarr),
                  'Dnu0':self.evparms.evfunc.get_DNU(fname=logf,arr=logarr) }
        
        if imcmc==1:
           ### MCMC we just take the last model which is the age we want 
           for key, item in get_evar.items():
              if key in model_observables.dtype.names: 
                 model_observables[key]=item[-1]  
              
           model_observables=self.eval_metrics(model_observables,obsdata)
           
        else:
            ### Need to create a new big array the length of nmod
            big=np.full( len(nmods), np.nan, dtype=model_observables.dtypes)
            for key, item in get_evar.items():
              if key in model_observables.dtype.names:   
                for i in range(len(nmods)):               
                   big[key][i]=item[i]            
            
            ### Evalutate and copy the smallest over 
            big=self.eval_metrics(big,obsdata)
            big=np.sort(big, order='MLE_total')
            model_observables=big[-1* self.parms.max_mods_osc:]
            
        return model_observables   
    
   
   
    def eval_metrics(self, arr, obsdata,osc_fail=None):
        """Here we calculate our metrics based on 
           the availible information passed in the array."""
        
        larr=len(arr)
        chi_ev=[0]*larr
        mle_ev=[1]*larr
        chi_osc=[0]*larr
        mle_osc=[1]*larr
        
        
        ### If the total array is np.nan this is our first time through
        if np.isnan(arr['chi_total'][0]):
           eval_cols=[x for x in arr.dtype.names if not np.isnan(np.atleast_1d(arr)[x][0])
                   and x in obsdata.obsdict.keys()]    
           
           ### Save for failed oscillation calculations. lol actally dont need it
           self.levcols=len(eval_cols)
           
           for col in eval_cols:
              chi_parm=[self.chi_sq(obsdata.obsdict[col],
                                    obsdata.obserr[col+'_ERR'],
                                    arr[col][i]) for i in range(larr)]   
        
              mle_parm = [self.weight_MLE(obsdata.obsdict[col],
                                    obsdata.obserr[col+'_ERR'],
                                    arr[col][i]) for i in range(larr)]   
                            
              if col =='Dnu0':
                  ### Put this in the array total column, not the temporary ev column
                  arr['chi_total']=chi_parm
                  arr['mle_total']=mle_parm
              else:
                 ### Keep including in the temporary ev column 
                 chi_ev=[x+y for x,y in zip(chi_ev, chi_parm)]
                 mle_ev=[x*y for x,y in zip(mle_ev, mle_parm)]
           
           
           ### Out of the loop. Set the temporary ev column in the array
           arr['chi_ev']=chi_ev
           arr['mle_ev']=mle_ev
           
           if 'Dnu0' in eval_cols:
               ### Dnu0 component already in the array total column 
               arr['mle_total']=[x*y for x,y in zip(arr['mle_total'], arr['mle_ev'])]
               arr['chi_total']=[x+y for x,y in zip(arr['chi_total'], arr['chi_ev'])] 
                                                                                                    
               if obsdata.freqfile:
                 ### Dnu0 not counted in the ev params normalization  
                 ### But we will count it in the array total chi which 
                 ### we overwrite later once we have seismic info
                 arr['chi_ev']=arr['chi_ev']/(self.levcols-1)   
                 arr['chi_total']=arr['chi_total']/self.levcols

               else:
                  ### No frequencies will be calculated later. 
                  ### DNU is considered a structure constraint so normalise and reset total chi 
                  arr['chi_total']=arr['chi_total']/self.levcols
                  arr['chi_ev']=arr['chi_total']    
               
           else:
               ### Easy case. No Dnu to deal with. Simply normalise and store 
               arr['mle_total']=arr['mle_ev']
               arr['chi_ev']=arr['chi_ev']/self.levcols
               arr['chi_total']=arr['chi_ev']
                        
        
        else:
            ### We now have oscillation data we want to evaluate.
            ### Asteroseismic observables are highly correlated. We need to use to covairance matrix
            ### See http://adsabs.harvard.edu/abs/2014A%26A...569A..21L or Press (1992)
            
            ### Have to only evaluate those we have successfully caclulated freqs for
            for i, row in enumerate(arr):
                if row['nmod'] in osc_fail:
                    ###Essentially now consider Dnu an Ev observable. Already normalised 
                    arr['chi_ev'][i]=arr['chi_total'][i]
                    arr['mle_ev'][i]=arr['mle_total'][i]
                    
                else:
                  data= [row[col] for col in obsdata.freqs.fcols] 
                  
                  arr['chi_osc'][i]= self.Cov_Chi(obsdata.freqs.inst,
                                                  obsdata.freqs.cov_matrix, 
                                                  data)
                  
                  arr['mle_osc'][i]=  self.Cov_MLE(obsdata.freqs.inst,
                                                  obsdata.freqs.cov_matrix, 
                                                  data)
            
                  arr['chi_total'][i]=arr['chi_osc'][i]+arr['chi_ev'][i]
                  arr['mle_total'][i]=arr['mle_osc'][i]+arr['mle_ev'][i]
                                       
        return arr    

    def get_osc_obs(self, logf, logarr, model_observables,obsdata,
                    osc_pass, osc_fail,warnfile, imcmc):
        """Add in the contribution from the seismic constraints"""
        
        models=[int(x) for x in model_observables['nmod']]
        
        ### Need the nu-max of each model 
        nmxs=self.evparms.evfunc.get_NU_MAX(arr=logarr)
        nmods=list(self.evparms.evfunc.get_mods(arr=logarr))
        nmxs=[nmxs[nmods.index(mod)] for mod in models] 
        
        
        for mod in osc_pass:
          idx=models.index(mod)
          freqs = self.osccode.oscfunc.get_freqs(mod)
          seismic_vars=osapi.get_seismic(obsdata.freqs.fcols, freqs, nmxs[idx])
          for i, col in enumerate(obsdata.freqs.fcols):
             model_observables[col][idx]=seismic_vars[i]                               

        ### Evaluate the metrics
        model_observables= self.eval_metrics(model_observables,obsdata,osc_fail)
       
        ### I dont have a catch all here if something goes wrong with the frequencies coming back
        ### Also should consider a report
       
        return model_observables 

        
    def chi_sq(self, obs, unc, mod):
        return (obs - mod)**2 / unc**2

    def weight_MLE(self, obs, unc, mod):
        return -(norm.logpdf(mod, loc=obs, scale=unc))

    def Cov_MLE(self, means, covs, data):
        """data are from models, means are from obs in our case"""
        var = multivariate_normal(mean=means, cov=covs)
        nll = var.logpdf(data)
        # print(nll)
        return -nll

    def Cov_Chi(self, means, covs, data):
        """data are from models, means are from obs in our case"""

        ### Verbose coding for clarity
        dif = [x - y for x, y, in zip(data, means)]
        a=np.matrix(covs).I
        b=np.atleast_1d(dif).T
        c=np.atleast_1d(dif) 
     
        chi=np.dot(b,a)
        chi=np.dot(chi,c)
        
        #chi = np.a(dif).T dot np.matrix(covs).I dot dif
        return chi[0]
    
