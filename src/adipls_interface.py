import os
import shutil
import itertools
import string
import subprocess as sp
import time
import numpy


class adipls (object):
    """Library of functions specifically for running Adipls Pulsation Package"""

    def runosc(self, oscmods, name, evparms, evlog, parms, imcmc):
        self.name = name
        self.evparms = evparms
        self.evlog = evlog
        self.parms = parms
        self.cwd = os.getcwd()
        self.imcmc = imcmc

        if parms.verbose:
          print('adipls oscmods', oscmods)

        ### Pass the models we want to process and return 
        ### the respective gong file locations
        gongfiles = self.get_gongs(oscmods)
        if parms.verbose:
          print('adipls gongs', gongfiles)

        ### Convert gong to AMDL. Return AMDL model folder
        AMDLpaths = self.convert_amdl(gongfiles)
        if parms.verbose:
          print('adipls AMDLPaths', AMDLpaths)

        ### Determine the evolutionary phase of the models we are running
        evphases = self.getphases(AMDLpaths)

        ### Remesh the AMDL file. Return successful AMDL file/path/evphases
        redist_fnames, AMDLpaths, evphases = self.adi_remesh(
            AMDLpaths, evphases)

        ### Run the Adipls jobs. Return model (e.g., '000007') of stars
        ### sucussfully executed
        adipass = self.adiplsin(redist_fnames, AMDLpaths, evphases, self.parms,self.imcmc)

        ### We don't know how long adipls jobs take. Need to monitor
        if self.imcmc == 1:
            monpass = self.monitor_local(adipass)

        ### Convert sucessfully completed ADIPLS jobs to ascii.
        self.setobs(monpass)
        passed = [int(x) for x in monpass]
        failed = [x for x in oscmods if x not in passed]
        return passed, failed


    def get_gongs(self, oscmods):
        """Get the required GONG files"""
        gongs = []
        for gfile in os.listdir('./GONGS'):
            if gfile.endswith('GONG') or gfile.endswith('gong'):
                if int(gfile[:-5]) in oscmods:
                    gongs.append(gfile)
        return gongs 

    def convert_amdl(self, gongfiles):
        """Convert GONG to AMDL"""
  
        ###Keep track of what we want to convert and what we actually do convert
        paths = []
        failed = []
 
        ### Create directories to process the files
        for gong in gongfiles: 
            sheep = 0
            path = os.path.join("AMDL", gong[0:-5])

            ### Best not to overwrite just in case
            while os.path.isdir(path):
                letter = next(self.alphabet)
                path = os.path.join("AMDL", gong[0:-5] + letter)
            
            paths.append(path)
            os.makedirs(path)
            os.makedirs(os.path.join(path, 'admin'))
          
            ### Execute silently   
            cmd = 'fgong-amdl.d'
            arg1 = 'GONGS/' + gong
            arg2 = path + '/' + gong[0:-5] + '.amdl'
            sp.call([cmd, arg1, arg2], stdout=sp.DEVNULL, stderr=sp.DEVNULL)
            time.sleep(0.5)
            
            ### Avoid race condition 
            while not os.path.isfile(arg2):
                time.sleep(1)
                sheep += 1

                ### 30 seconds should be long enough 
                if sheep >= 30:
                    failed.append(path)
                    print(self.name, gong[0:-5], 'failed in convert Gongs. Removing')   
                    break 

        ### Ideal case everything worked
        if len(failed) == 0:
            if self.parms.verbose:
                print('GONGS Converted', self.name)
            return paths

        else:
            ### Remove their AMDL directory.  
            for path in failed:
                shutil.rmtree(path, ignore_errors=True)
                paths.remove(path)
                if self.parms.verbose:
                    print('removing', path, 'from AMDLpath')
            return paths


    def getphases(self, AMDLpaths):
        """Need to decide on parms for ADIPLS based on ev state.
           Use Integer to distinguish phase.
           Only consider MS from everything else right now"""

        converted_mods = [int(x.split('/')[-1]) for x in AMDLpaths]
        evXcentre = self.evparms.evfunc.get_Xcentre(self.evlog)
        phase = [0 if x > 0.01 else 1 for x in evXcentre]
        return phase


    def adi_remesh(self, AMDLpaths, phases):
        """Remesh the AMDL based on Ev phase"""
        redist_fnames = []
        failed = []

        ### Set the parameters we want based on the evolutionary phase
        rparms = {0: ['6202', '21'], 1: ['35202', '24']}

        ### Set the file name based on the number of points
        for i, dname in enumerate(AMDLpaths):
            inf = dname + '/' + dname[5:] + '.amdl'
            outf = dname + '/' + dname[5:] + '.amdl' + rparms[phases[i]][0]
            parms = (inf, outf, rparms[phases[i]][0], rparms[phases[i]][1])
            jobstring = """
2 '{}'    @
3 '{}'    @
-1 ''        @
nn,icnmsh
{},,,  @
icase,icvzbn,nsmth,ndisc,dlxdsc,dlgrmx,cacvzb
{}   ,      ,     ,0.001  ,      ,5.    ,      ,@
cg,  cx  ,ca  ,sig1,sig2,lmax,alphsf,adda4,accrm
 1., 0.05,0.05,0.  ,0.  ,2   ,      ,0.02 ,0.01    ,  @
nout,cn,irsu,unew
60,,,,,,,,,  @
nmodel,kmodel,itsaml,ioldex
,,,,,,,,,,,  @""".format(*parms)


            ### Write the redistb input file 
            fname = os.path.join(dname + "/", 'redistrb.in')
            with open(fname, 'w') as n:
                n.write(jobstring)

            ### Run silently on all the files  
            sheep = 0
            cmd = 'redistrb.c.d'
            arg = fname
            sp.call([cmd, arg], stdout=sp.DEVNULL, stderr=sp.DEVNULL)
            time.sleep(0.5)

            ### Wait 30 seconds to see if an outfile appears  
            while not os.path.isfile(outf):
                time.sleep(1)
                sheep += 1
                if sheep >= 30:
                    failed.append(dname)
                    print(self.name, dname, 'failed in Redisrb. Removing')

                    break

            if os.path.isfile(outf):
                redist_fnames.append(outf)

        if len(failed) == 0:
            if self.parms.verbose:
                print('AMDLs remeshed', self.name)
            return (redist_fnames, AMDLpaths, phases)

        ### Remove the failed occurances 
        else:
            for paths in failed:
                shutil.rmtree(path, ignore_errors=True)
                idx = AMDLpaths.index(path)
                AMDLpaths.remove(path)
                del phases[idx]
            return (redist_fnames, AMDLpaths, phases)



    def adiplsin(self, redist_fnames, AMDLpaths, evphases, parms, imcmc):
        adisub = []
        adict = {0: ['2', '2', '5000', '10000', '5', '1'],
                 1: ['2', '2', '20000', '10000', '5', '1']}
        dname = [x.split('/')[-1] for x in AMDLpaths]

        ### What ell do I want to calculate up to
        if imcmc==1: nsel = 4

        ### Get the models and Nu Max so to estimate their cut-off frequency 
        mods = self.evparms.evfunc.get_mods(self.evlog)
        mods = [int(x) for x in mods]
        nmxs = self.evparms.evfunc.get_NU_MAX(self.evlog)
  
 
        for i, path in enumerate(AMDLpaths):
            idx = mods.index(int(dname[i]))
            infile = redist_fnames[i].split('/')[-1]
             
            if imcmc==1:
                fmtargs = ("AMDL/" + dname[i] + '/' + infile,
                           "AMDL/" + dname[i] + '/' + dname[i] + '.agsm',
                           "AMDL/" + dname[i] + '/' + dname[i] + '.ssm',
                           str(nsel)) + tuple(adict[evphases[i]]) + (str(round(1.7 * nmxs[idx],
                                                                               2)),
                                                                     )

             


            jobstring = """
 2  '{}'   @
 9  '0'   @
 11 '{}'   @
 4
 15 '{}'   @
 -1 ''   @
  cntrd,
mod.osc.cst.int.out.dgn     @

mod:
  ifind,xmod,imlds,in,irname,nprmod,
       ,    ,     ,  ,      ,      ,  @
  ntrnct,ntrnsf,imdmod,
       ,       ,      , @
osc:
  el,nsel,els1,dels,dfsig1,dfsig2,nsig1,nsig2,
    ,    {},  0,   1,    0,       ,    1,    1,   @
  itrsig,sig1,istsig,inomde,itrds,
       1,   5,     1,     {},    , @
  dfsig,nsig,iscan,sig2,
       ,  {},   {}, {},     @
  eltrw1, eltrw2, sgtrw1, sgtrw2,
       0,     -1,     0,       -1,  @
cst:
  cgrav
  6.672320e-8               @
int:
  iplneq,iturpr,icow,alb,
  0,0,0,1,,,,,,             @
  istsbc,fctsbc,ibotbc,fcttbc,
  1,0,0,0,,,,,  @
  mdintg,iriche,xfit,fcnorm,eps,epssol,itmax,dsigre,
  {},{},0.99,,,,15,0,,,,,,  @
  fsig,dsigmx,irsevn,xmnevn,nftmax,itsord
  0.001,0.1,2,0,1,20,,,,,  @
out:
  istdpr,nout,nprcen,irsord,iekinr
  9,50,100,20,0,,,,,,,,     @
  iper,ivarf,kvarf,npvarf,nfmode,
  1,1,2,0,,,,,,,,,     @
  irotkr,nprtkr,igm1kr,npgmkr,ispcpr,
  0,,0,,0,,,,,,,     @
  icaswn, sigwn1, sigwn2, frqwn1, frqwn2,iorwn1, iorwn2, frlwn1, frlwn2
   ,     0,      -1,      0,  {}    ,  0,     -1,      0,     -1,   @
dgn:
  itssol,idgtss,moddet,iprdet,npout
  0,0,0,0,,,,,,,,,,,     @
  imstsl,imissl,imjssl,idgnrk
  ,,,,,,,,,    @
""".format(*fmtargs)



            ff = os.path.join("AMDL/" + dname[i], 'adipls.in')
            with open(ff, 'w') as h:
                h.write(jobstring)

            if parms.verbose:
                print('adipls files made', self.name)


            if imcmc == 1:
               ### Run and execute without cluster script
               cmd = 'adipls.c.d'
               for i, dn in enumerate(dname):
                   arg1 = "AMDL/" + dname[i] + '/adipls.in'
                   with open('AMDL/' + dname[i] + '/adi.log', 'w') as aof:
                      sp.Popen([cmd, arg1], stdout=aof, stderr=aof)
                   adisub.append(dn)
               time.sleep(5)
               return adisub




    def monitor_local(self, adipass):
        """Can't remember what I was thinking when I wrote this""" 
        mon_pass = []
        mods = adipass
        sheep = 0
        while len(mods) > 0:
            for i, mod in enumerate(adipass):
                if mod in mods:
                    logf = 'AMDL/' + mod + '/adi.log'
                    with open(logf, 'r') as ll:
                        out = ll.readlines()
                    if len(out) > 0:
                        outl = out[-1].strip().split()
                        if len(outl) > 0:
                            if outl[0] == 'return':

                                if outl[-1] == '0':
                                    mon_pass.append(mod)
                                else:
                                    print(
                                        self.name, adipass[i], 'failed in Monitor ADIPLS')

                                mods.remove(mod)
                            else:
                                if sheep == 60:
                                    print(
                                        self.name, adipass[i], 'failed in Monitor ADIPLS')
                                    mods.remove(mod)
                        else:
                            if sheep == 60:
                                print(
                                    self.name, adipass[i], 'failed in Monitor ADIPLS')
                                mods.remove(mod)
                    else:
                        """Hasnt finished"""
                        if sheep == 60:
                            print(
                                self.name,
                                adipass[i],
                                'failed in Monitor ADIPLS')
                            mods.remove(mod)

            sheep += 1
            time.sleep(5)
        return mon_pass      


    def setobs(self, model_folders):
        cmd = 'set-obs.d'
        arg1 = '15'
        for folder in model_folders:
            # if int(folder) not in self.failed:
            arg2 = 'AMDL/' + folder + '/' + folder + '.agsm'
            arg3 = 'AMDL/' + folder + '/' + folder + '.ascii'
            with open('AMDL/' + folder + '/setobs.log', 'w') as aof:
                sp.run([cmd, arg1, arg2, arg3], stdout=aof, stderr=aof)
        time.sleep(5)    
        
        
        
    def get_freqs(self, folder):
        modname = str(folder).zfill(7)
        fname = 'AMDL/' + modname + '/' + modname + '.ascii'
        return numpy.genfromtxt(
            fname, usecols=[0, 1, 2, 3], 
              dtype=[('l', int), ('n', int), ('v', float), ('mi', float)])   



    def make_run_dir(self):
        try:
            if not os.path.isdir("AMDL"):
                os.makedirs("AMDL")
        except FileExistsError:
            pass
        return os.path.join(os.getcwd(), 'AMDL')
     
