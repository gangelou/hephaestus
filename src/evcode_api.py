import numpy
from src.ggutils import InitialModels, writer
import shutil
import os
import re
import subprocess as sp
import math

class evcode(object):
    """ Class that controls how to treat the Evolution codes. Each code must have a defintion of how to perform each function listed here """

    def __init__(self, parm):
        self.name = parm.evcode
        self.parm = parm
        if parm.evcode.lower() == 'gars':
            from src.gars_interface import garstec as evfunc
        self.ev = evfunc()
        if self.parm.verbose:
            print('loaded Evolution code functions for ', parm.evcode.lower())

    def getzx(self):
        """Method to determine the value of Z/X for the respective Evolution code"""
        return self.ev.getzx(self.parm)

    def readlog(self, fname):
        """Method to read the evolutionary log file for the respective Evolution code"""
        return self.ev.readlog(fname)

    def get_Z(self, fname=None, arr=None):
        """Method to determine surface Z inthe respective Evolution code"""
        return self.ev.get_Z(fname=fname, arr=arr)

    def get_aMLT(self, gparms):
        """Method to determine the value of alpha MLT in respective Evolution code param file"""
        return self.ev.get_aMLT(gparms)

    def get_initial_model_list(self, arr, dest_folders):
        """Method to find and extract the starting model for the respective Ev code"""
        return self.ev.get_initial_model_list(arr, dest_folders, self.parm)

    def get_initial_model(self, arr, dest_folders,idx=0):
        """Method to find the ebst starting models"""
        return self.ev.get_initial_model(arr, dest_folders, self.parm, idx=idx)

    def copy_runfiles(self, dest_folders, evparms):
        """Method to copy the run files to required directories"""
        self.ev.copy_runfiles(dest_folders, evparms, self.parm)

    def runev(self, arr, obsdata, parms, evparm, warnf, monitor):
        """Method to execute and run the stellar evolution code"""
        return self.ev.runev(arr, obsdata, parms, evparm, warnf, monitor)

    def get_TEFF(self, fname=None, arr=None):
        """Method to get the Teff from ev output"""
        return self.ev.get_Teff(fname=fname, arr=arr)

    def get_FE_H(self, fname=None):
        """Method to get the [Fe/H] from ev output"""
        return self.ev.get_FeH(self.parm.zx,fname=fname)

    def get_LOG_G(self, fname=None, arr=None):
        """Method to get log g from ev output"""
        return self.ev.get_log_g(fname=fname, arr=arr)

    def get_NU_MAX(self, fname=None, arr=None):
        """Method to get Nu Max from ev output"""
        return self.ev.get_nu_max(self.parm.nmsun, self.parm.tsun, fname=fname, arr=arr)

    def get_MASS(self, fname=None, arr=None):
        """Method to get Nu Max from ev output"""
        return self.ev.get_mass(fname=fname, arr=arr)

    def get_LUM(self, fname=None, arr=None):
        """Method to get Nu Max from ev output"""
        return self.ev.get_lum(fname=fname, arr=arr)

    def get_mods(self, fname=None, arr=None):
        """Method to get list of models from ev output"""
        return self.ev.get_mods(fname=fname, arr=arr)

    def get_Xcentre(self, fname=None, arr=None):
        """Method to get central Hydrogen abundance"""
        return self.ev.get_Xcentre(fname=fname, arr=arr)

    def get_AGE(self, fname=None, arr=None):
        """Method to get central Hydrogen abundance"""
        return self.ev.get_AGE(fname=fname, arr=arr)

    def archlog(self, fname, zxsun):
        """Method to get archival data for storage"""
        return self.ev.archlog(fname, zxsun)

    def get_DNU(self, fname=None, arr=None):
        """Here is where the DNU stuff should go"""
        FeH  = self.get_FE_H(fname=fname)
        Teff = self.get_TEFF(fname=fname, arr=arr)
        Mass = self.get_MASS(fname=fname, arr=arr)
        Lum  = self.get_LUM(fname=fname, arr=arr)

        """Scaling Relation Correction from Elisabeth Guggenberger.
            Delta nu = ( (M/Msun)^0.5 * (Teff/Teffsun)^3 *Deltanu_Sun)/ (L/Lsun)^0.75)
            There is also the power law form from nu max dnu=[0.263*i**0.772 for i in nmx]
            (param[0]*feh+param[1])*exp^((param[2]*feh+param[3])*Teff/10000.)*(cos(param[4]*Teff/10000.+(param[5]*feh^2+param[6]*feh+param[7])))
            +(param[8]+param[9]*feh)"""

        func_coeffs = [0.55757590, 1.1315471, -0.94266469, 1.9988931,
                       21.135114, 0.17652742, 0.42097644, 0.75068501, 134.92490, 0.69975320]
        reference = [func_coeffs[0] *
                     FeH[i] +
                     func_coeffs[1] *
                     math.exp((func_coeffs[2] *
                               FeH[i] +
                               func_coeffs[3]) *
                              Teff[i] /
                              10000.) *
                     (math.cos(func_coeffs[4] *
                               Teff[i] /
                               10000. +
                               (func_coeffs[5] *
                                FeH[i]**2 +
                                func_coeffs[6] *
                                FeH[i] +
                                func_coeffs[7]))) +
                     (func_coeffs[8] +
                      func_coeffs[9] *
                      FeH[i]) for i in range(len(Teff))]

        DNU = [(Mass[i]**0.5 * (Teff[i] / self.parm.tsun)**3 *
                reference[i]) / Lum[i] for i in range(len(Teff))]
        return DNU


class ev_parmfile(object):

    def __init__(self, evcode, evdir, lf='./log.xxx', **kwargs):
        if evcode.lower() == 'gars':
            from src.gars_interface import gars_parmfile
            self.evparm = gars_parmfile(evdir, lf, **kwargs)
