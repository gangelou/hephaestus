import sys
import ast
import distutils.util
import os
from itertools import permutations, product
import string
import src.ggutils as sut
import shutil

"""Code to use machine learning libraries to characterise stars. 
   Most of this is shamelessly ripped off from Earl's original code."""
class parameters(object):

   """Parameter file class"""
   def __init__(self, pfile):

      ### First define each of our parameters and their type
      strings = (['run_dir',
                  'obs_dir', 
                  'garsdir',
                  'gars_inputs',
                  'initial_models',
                  'abunds',
                  'evcode',
                  'osccode',
                  'local_search_algorithm',
                  'obs_pattern',
                  'freq_pattern',
                  'starting_pattern' 
                  ])

      ints=(['sobol_skip',
             'nwalkers',
              'save_chain_len',
              'iter_restart',
              'burn_in',
              'z_free'])

      reals = (['nmsun',
               'dnusun',
               'd02sun',
               'tsun',
               'bigg',
               'y0_fixed',
               'dydz',
               'alpha_mlt',
               'overshoot'])

      boolean =(['verbose','restart','radial_order'])

      tups = ['mrange', 'fehrange','zrange','hrange','yrange','orange']
      lists = ['stop_criteria', 'stop_value', 'stop_eps', 'gkeys']


      self.tups = tups

      ### Read in parameter file and attribute 
      with open(pfile) as f:
         for line in f:
            #print(line) 
            if line.lstrip().startswith('#') or not line.strip():
               continue
            else:
               line = line.split('#')
               key, var = line[0].split('=')
               key = key.strip().lower()
               #print(key,var)
               if key in strings:
                   var=var.replace("'", "")
                   var=var.replace('"','')
                   var=var.strip()
                   
                   setattr(self, key, var)

               elif key in boolean:
                    setattr(self, key, 
                            bool(distutils.util.strtobool(
                                        var.strip())))
                          
               elif key in ints:
                    setattr(self, key, int(var.strip()))  

               elif key in lists:
                    setattr(self, key, ast.literal_eval(var.strip()))


               elif key in reals:
                   setattr(self, key, float(var.strip()))
                   
               elif key in tups:
                    setattr(self, key, ast.literal_eval(var.strip()))

               else:
                    print( "Warning: Can't find parameter in the lists of types (in get_parms)- ", key)
                    print("Stopping")
                    sys.exit(2)

      ### Test we have everything we need
      ndef=[] 
      for param in strings+ints+boolean+lists+tups+reals:
         try: getattr(self, param)
         except: ndef.append(param)

      if len(ndef) > 0:
         print('Error - Required Parameters not defined')
         print(ndef)  
         sys.exit(2)


      ### Various tasks
      afiles={'AGS09': 'Abundances_solar_A09Pho.dat', 
              'GN93': 'Abundances_solar_GN93.dat',  
              'GS98': 'Abundances_solar_GS98.dat'}
      
      self.abund_file=os.path.join(self.gars_inputs,'Abundances', afiles[self.abunds])
      
      ### Probably asking for trouble here. 
      shutil.copy(self.abund_file, os.path.join(self.garsdir,'Abundances.dat'))


   def set_dir(self):
        """Create the initial Directory"""
        self.rund = self.run_dir
        self.run_dir = 'run/' + self.run_dir

        if os.path.exists(self.run_dir):
            print('Run directory Already exists')
        else:
            print('New Directory Created - ', self.run_dir)
            os.makedirs(self.run_dir)
            os.makedirs(self.run_dir + '/tracks')
            
            
   def create_log(self):
      """Decide on name and location of parent logfile"""
      
      def check_lfexist(ad, lf):
            alph = product(string.ascii_lowercase)
            logf = os.path.join(ad, lf + '.log')
            while os.path.isfile(logf):
                logf = os.path.join(ad, lf + str(next(alph)[0]) + '.log')
            return logf
        
      admin_dir = 'run/' + self.rund + '/admin/'
      if not os.path.exists(admin_dir):
        os.makedirs(admin_dir)

      ### Keep it simple for now
      name = 'run_' + self.st
      self.runname = name
      self.logfile = check_lfexist(admin_dir, name)
      return self.logfile
  
   def determine_free_parms(self):
        """List of my free parameters. Nothing complicated for now """
        free = ['M','Y','Z']
        return free
  
   def generate_sobol_map(self):
        """Create a mapping of dimensions"""
        self.maps = []

        for p in permutations(self.free_parms):
            self.maps.append(p)

        if self.verbose:
            print(self.maps)  
            
   def check_codes(self, parms):
        """Make sure we have a valid option for the stellar evolution code"""
        evcodes = ['gars']
        osccodes = ['adip']

        if parms.evcode.lower() not in evcodes:
            sut.writer(
                parms.logfile,
                'check_codes',
                'ERR ',
                'Invalid Stellar Evolution Code',
                'Options are:' +
                str(evcodes))
            print('Invalid Stellar Evolution Code')
            sys.exit(2)

        else:
             sut.writer(
                parms.logfile,
                'check_codes',
                'INFO',
                'Using Stellar Evolution Code -- ' +
                parms.evcode.lower())

        if parms.osccode.lower() not in osccodes:
            sut.writer(
                parms.logfile,
                'check_codes',
                'ERR ',
                'Invalid Oscillation Evolution Code',
                'Options are:' +
                str(osccodes))
            print('Invalid Stellar Oscillation Code')
            sys.exit(2)
        else:
            sut.writer(
                parms.logfile,
                'check_codes',
                'INFO',
                'Using Stellar Oscillation Code -- ' +
                parms.osccode.lower())            



   def check_ranges(self, parms):
        """Make sure the search ranges provided are ok """
        test = [parm for parm in parms.tups]

        for parm in test:
            tup = getattr(parms, parm)
            self.arange = (1.6, 2.2)
            # If both negative set to reasonable search ranges
            # 'mrange','zrange','hrange','yrange'
            if tup[0] < 0 and tup[1] < 0:
                if parm == 'mrange':
                    self.mrange = (0.7, 2.0)
                elif parm == 'zrange':
                    self.zrange = (0.001, 0.025)
                elif parm == 'hrange':
                    self.hrange = (0.68, 0.78)
                elif parm == 'yrange':
                    self.yrange = (0.245, 0.35)

            # If we have one negative and one positive value, Stop.
            if parm != 'fehrange':
              if (tup[0] < 0 and tup[1] > 0) or (tup[0] > 0 and tup[1] < 0):
                print(
                    'Check ',
                    parm,
                    ' in config file. One neg and one positive value')
                sut.writer(parms.logfile, 'check_ranges', 'ERR ', 'Check ' +
                       parm + ' in config file. One neg and one pos value')
                sys.exit(1)

        # make sure range increases. Decreasing range my indicate a typo in
        # config file. better safe than sorry.
            if tup[0] >= 0 and tup[1] > 0:
                if tup[0] > tup[1]:
                    print(
                        'Check ',
                        parm,
                        ' in config file. Range not increasing - could be a typo in config')
                    sut.writer(
                        parms.logfile,
                        'check_ranges',
                        'ERR ',
                        'Check ' +
                        parm +
                        ' in config file.')
                    sut.writer(parms.logfile, 'check_ranges', 'ERR ',
                           'Range not increasing - could be a typo in config')
                    sys.exit(1)

        # Set zrange to (-1,-1) if z_free is false.
        if not parms.z_free:
            if parms.zrange[0] >= 0 or parms.zrange[1] > 0:
                print('ignoring z range as z_free is false')
                sut.writer(parms.logfile, 'check_ranges', 'WARN',
                       'Ignoring zrange as z_free is False')
                parms.zrange = (-1.0, -1.0)

        # H range must be > 1
        if parms.hrange[0] >= 0 and parms.hrange[1] > 0:
            if parms.hrange[1] > 1:
                print(
                    'hrange=',
                    parms.hrange,
                    'Check hrange in config file. X(H) cannot exceed 1')
                sut.writer(
                    parms.logfile,
                    'check_ranges',
                    'ERR ',
                    'Check hrange in config file')
                sut.writer(
                    parms.logfile,
                    'check_ranges',
                    'ERR ',
                    'X(H) cannot exceed 1')
                sys.exit(1)

            if parms.hrange[1] > 0.8 or parms.hrange[0] < 0.5:
                print('Unrealistic Hrange. Asking for trouble but continuing')
                sut.writer(parms.logfile, 'check_ranges', 'WARN',
                       'Unrealistic Hrange. Asking for trouble but continuing')
                
                

   def check_algorithms(self, parms):
        """ Check that the Algorithm parameters make sense """
        
        #if parms.local_search_algorithm not in ["MCMC", "DHS", "NONE"]:
        if parms.local_search_algorithm not in ["MCMC", "DHS", "NONE"]:
            sut.writer(
                parms.logfile,
                'check_alogorithms',
                'ERR ',
                'Invalid LOCAL Search Algorithm',
                'Options are: MCMC, DHS, NONE')
            sys.exit(1)

        
        ### for future reference 
        """
        if parms.global_search_algorithm not in ["MCMC", "GA", "DHS", "NONE"]:
            sut.writer(
                parms.logfile,
                'check_alogorithms',
                'ERR ',
                'Invalid GLOBAL Search Algorithm',
                'Options are: MCMC, GA, DHS, NONE')

            sys.exit(1)


        if parms.global_search_algorithm == parms.local_search_algorithm:
            sut.writer(
                parms.logfile,
                'check_alogorithms',
                'WARN ',
                'Local and Global algorithms are the same. Ignoring Local Option') """
                
