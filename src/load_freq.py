import xml.etree.ElementTree as ET
import numpy as np
import src.ggutils as sut
from src.osccode_api import get_seismic

class load_freqs(object):
   def __init__(self, ffile, parm):

     if ffile.endswith('.xml'):
       freqs_found = self.kleg_xml(ffile)
       self.fmt=0   
     elif ffile.endswith('.dat'):
       freqs_found = self.dat_ascii(ffile)    
       self.fmt=1
     elif ffile.endswith('.ascii'):
       freqs_found=self.adipls_ascii(ffile)
       self.fmt=2
     else: 
        sut.writer(parm.logfile,
                  'load_freqs',
                  'ERR',
                   ffile +
                   ' -- No method for reading frequencies/ratios in this format',
                   ' Check the file format is .xml, .dat or .ascii')
        sys.exit(2) 

     
     if not freqs_found:
        sut.writer( parm.logfile,
                    'get_frequencies',
                    'ERR',
                    ffile +
                    ' -- Cannot Find Freq file or Read in Data',
                    ' Set verbose=true for detailed information')
        sys.exit(2)
     else:
       sut.writer( parm.logfile,
                    'get_frequencies',
                    'INFO',
                    ffile +
                    ' Frequencies read in with',
                     str(len(self.modes)) + ' modes detected')


   def kleg_xml(self, fname):
        tree = ET.parse(fname)
        root = tree.getroot()
        rows = len(root.findall('frequency_ratio'))
        self.ratios = np.zeros(rows, dtype=([('ID', 'i4'), ('rtype', (str, 5)), (
            'order', 'i4'), ('val', 'f8'), ('serr', 'f8'), ('errp', 'f8'), ('errm', 'f8')]))

        for i, ratio in enumerate(root.findall('frequency_ratio')):
            self.ratios['ID'][i] = ratio.get('ID')
            self.ratios['serr'][i] = ratio.get('error')
            self.ratios['errp'][i] = ratio.get('error_plus')
            self.ratios['errm'][i] = ratio.get('error_minus')
            self.ratios['order'][i] = ratio.get('order')
            self.ratios['rtype'][i] = ratio.get('type')
            self.ratios['val'][i] = ratio.text

        rows = len(root.findall('mode'))
        self.modes = np.zeros(rows, dtype=([('ID', 'i4'), ('n', 'i4'), (
            'l', 'i4'), ('v', 'f8'), ('serr', 'f8'), ('errp', 'f8'), ('errm', 'f8')]))

        for i, mode in enumerate(root.findall('mode')):
            self.modes['ID'][i] = mode.get('ID')
            self.modes['n'][i] = mode.find('order').text
            self.modes['l'][i] = mode.find('degree').text
            frequency = mode.find('frequency')
            self.modes['v'][i] = frequency.text
            self.modes['serr'][i] = frequency.get('error')
            self.modes['errp'][i] = frequency.get('error_plus')
            self.modes['errm'][i] = frequency.get('error_minus')

        rows = len(root.findall('mode_corr'))
        self.modecov = np.zeros(rows, dtype=([('d1', 'i4'), ('o1', 'i4'), ('ID1', 'i4'), (
            'd2', 'i4'), ('o2', 'i4'), ('ID2', 'i4'), ('cov', 'f8'), ('pearson', 'f8')]))

        for i, corr in enumerate(root.findall('mode_corr')):
            self.modecov['d1'][i] = corr.find('degree1').text
            self.modecov['o1'][i] = corr.find('order1').text
            self.modecov['ID1'][i] = corr.find('ID1').text
            self.modecov['d2'][i] = corr.find('degree2').text
            self.modecov['o2'][i] = corr.find('order2').text
            self.modecov['ID2'][i] = corr.find('ID2').text
            freq = corr.find('frequency_corr')
            self.modecov['pearson'][i] = freq.text
            self.modecov['cov'][i] = freq.get('covariance')

        rows = len(root.findall('frequency_ratio_corr'))
        self.ratiocov = np.zeros(rows, dtype=([('o1', 'i4'), ('type1', (str, 4)), ('ID1', 'i4'), (
            'o2', 'i4'), ('type2', (str, 4)), ('ID2', 'i4'), ('cov', 'f8'), ('pearson', 'f8')]))

        for i, corr in enumerate(root.findall('frequency_ratio_corr')):
            self.ratiocov['o1'][i] = corr.find('order1').text
            self.ratiocov['type1'][i] = corr.find('type1').text
            self.ratiocov['ID1'][i] = corr.find('ID1').text
            self.ratiocov['o2'][i] = corr.find('order2').text
            self.ratiocov['type2'][i] = corr.find('type2').text
            self.ratiocov['ID2'][i] = corr.find('ID2').text
            freq = corr.find('ratio_corr')
            self.ratiocov['pearson'][i] = freq.text
            self.ratiocov['cov'][i] = freq.get('covariance')

        # Should have frequencies if you want frequency ratios. For this file
        # format anyway.
        if i > 0:
            return True
        else:
            return False

   def dat_ascii(self, fname):

         try:
           freq_data=np.genfromtxt(fname, delimiter='', dtype=[int,int,float, float], names=True) 
         except: freq_data=np.genfromtxt(fname, delimiter=',', dtype=[int,int,float, float], names=True)
         dnames={'nu':'v', 'dnu':'err'}
         nnames= freq_data.dtype.names
         names=[ dnames[x] if x in dnames else x for x in nnames ]
         freq_data.dtype.names=names
         self.modes=freq_data
         if len(self.modes) > 0:
            return True
         else:
            return False


   def adipls_ascii(self, fname):

       ascii=np.genfromtxt(
            fname, usecols=[
                0, 1, 2, 3], dtype=[
                ('l', int), ('n', int), ('v', float), ('mi', float)])
 
       rows= len(ascii)
       self.modes = np.zeros(rows, dtype=([('ID', 'i4'), ('n', 'i4'), (
            'l', 'i4'), ('v', 'f8'), ('serr', 'f8'), ('errp', 'f8'), ('errm', 'f8')]))
       self.modes['ID']=[x for x in range(rows)]
       self.modes['n']= ascii['n']
       self.modes['l']=ascii['l']
       self.modes['v']=ascii['v']
       self.modes['serr']=[0.15 if ascii['l'][i]==0 else 0.5 for i in range(rows)]             
       if len(self.modes) > 0:
            return True
       else:
            return False

   def load_seismic_obs(self, modes, numax,parms):
       """Get separations, ratios etc"""

       ### Lets work out what degrees we are working with
       lk2=['Dnu0', 'dnu02', 'dnu13', 'r02','r01','r13','r10']
       fcols=[x for x in lk2 if '0' in x or '1' in x]    
       if 3 not in modes['l']:
           fcols=[x for x in fcols if '3' not in x]
       if 2 not in modes['l']:
           fcols=[x for x in fcols if '2' not in x]         
       if 1 not in modes['l']:
           fcols=[x for x in fcols if '1' not in x] 
       

       ### This is a pass through once to work out which columns we need to calculate.
       ### Pertains to the case when we want individual ratios
       if parms.radial_order == True:
           rdict=get_freqs(fcols, modes, numax['value'], get_rs=True) 
           for key, olist in rdict.items():
               for nord in olist:
                  par= key+'_'+str(nord)
                  fcols.append(par)

       if parms.verbose:
           print(fcols) 
       
       ### Not sure how we should store it. List and dictionary for now
       self.fcols=fcols
       self.inst=get_seismic(fcols, modes, numax['value'])
       #self.freq_consts={k:v for k,v in zip(self.fcols,self.inst)}
       
       ### Should probably work out covariance matrix for seismic data
       instantions=1000
       nmxs= np.random.normal(numax['value'],numax['uncertainty'],instantions)
       results2= list(map(self.rand_instf,nmxs)) 
       self.corr_matrix=np.corrcoef(np.asarray(results2).T)
       self.cov_matrix=np.cov(np.asarray(results2).T)
       
       ### Define some errors for our seismic constraints
       self.ferrs={}
       self.freq_consts={}
       for i, col in enumerate(self.fcols):
           self.freq_consts[col]=np.asarray(results2)[:,i].mean()
           self.ferrs[col+'_ERR']=np.asarray(results2)[:,i].std()

       
   def rand_instf(self,nmx): 
     ### Being a bit naughty and leaving some checks out. 

     inst=np.array([-1]*len(self.fcols))
     lfd=len(self.modes['l'])
     fd2=np.full((lfd),-1, dtype=[('l','int'),('n','int'),('v','float')])
     fd2['l']=self.modes['l']
     fd2['n']=self.modes['n']
     np.random.seed()
     fd2['v']=[np.random.normal(self.modes['v'][i],self.modes['err'][i]) for i in range(lfd)]
     inst=get_seismic(self.fcols, np.asarray(fd2), nmx)
     return inst  



  
