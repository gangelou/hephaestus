import matplotlib
matplotlib.use('pdf')
import corner
import emcee
from matplotlib.ticker import MaxNLocator
import multiprocessing as mp
import numpy as np
import os
import pandas as pd
import pylab as pl
from src.ggutils import writer,swriter,ylabels
from src.calc_model import structure
import shutil
import sys
import math
class mcmcStar(object):

    def __init__(self, obsdata, parms, evparms):
        self.obsdata=obsdata
        self.parms=parms
        self.evparms=evparms
        self.name=self.obsdata.name               
        self.chains=None

        self.run_out=os.path.join(parms.run_dir, 'tracks')
        self.star_out=self.obsdata.root.replace(self.parms.obs_dir, self.run_out)
        
        self.MCMCdir=os.path.join(self.star_out, 'MCMC')
        self.MCMClogdir=os.path.join(self.star_out,'logsMCMC')
        self.MCMClogf=os.path.join(self.MCMClogdir, self.obsdata.name+'_mcmc.log')
        
        temp=self.MCMClogf.split('/')
        self.rellogf='../../../'+temp[-2]+'/'+temp[-1]
        
        if not os.path.isdir(self.MCMCdir):    os.makedirs(self.MCMCdir)
        if not os.path.isdir(self.MCMClogdir): os.makedirs(self.MCMClogdir)
        

        ### Set the number of walkers     
        if self.parms.nwalkers < 0:
            cores=mp.cpu_count()
            self.nwalkers = max(cores-2,2)
        else:
          self.nwalkers =  self.parms.nwalkers

        ### This list defines our model  
        self.model={} 
        for key in ['M', 'Y', 'Z', 'alpha','overshoot', 'age']:
          self.model[key]=''


        ### Output for debuggging
        if parms.verbose:
           self.print_vars()
 
        ### Lets begin
        print('MCMC Now Processing', self.name)
        writer(self.MCMClogf, 'mcmcStar', 'INFO', 'Now Beginning MCMC for:',
               'Star ' + str(self.name))
       
        ### Set limits to the search
        self.set_parm_limits(self.parms)
      
        ### Do we have a restart file.
        self.restart=False  
        npzs = [os.path.join(self.MCMClogdir,x) for x in os.listdir(self.MCMClogdir) if x.endswith('.npz')]
        
        ### Initialise the arrays. A few Routines called from within intialise. 
        pos, prob, rstate, generate, ndim, init_mod=self.intialise_mcmc(obsdata, npzs, parms)
        self.run_mcmc(pos, prob, rstate, generate, ndim, init_mod, self.nwalkers, self.obsdata)
        
        
        print('Finished MCMC for ', self.name)
        writer(self.MCMClogf, 'mcmcStar', 'INFO', 'Finished MCMC for:',
               'Star ' + str(self.name))


    def intialise_mcmc(self, obsdata, npzs, parms):
        """Load the arrays and model"""

        ### First priority it to load npz file
        if len(npzs) > 0:
          if len(npzs)==1: 

             pos, prob, rstate, generate, self.init_mod=self.npz_start(npzs[0])
             ndim=len(generate)
             
             print('Loading npz restart file', npzs[0])
             writer(self.MCMClogf, 'mcmcStar', 'INFO', 'Loaded npz restart file:', npzs[0])
             writer(parms.logf, 'mcmcStar', 'INFO', 'Loaded npz restart file:', npzs[0])
             self.restart=True
   
          else:
             print('Found multiple npz restart files', npzs)
             writer(self.MCMClogf, 'mcmcStar', 'ERR ', 'Mutiple npz restart files found in:',self.MCMCdir)
             writer(parms.logf, 'mcmcStar', 'ERR ', 'Mutiple npz restart files found in:',self.MCMCdir)
             sys.exit(2)

 
        ### Then we try to load a starting model and if all else fails we guess. 
        ### Reminder: Emptry strings are Falsey
        if not self.restart:
           if obsdata.startfile:
              print('Found a starting file',  obsdata.startfile)
              writer(self.MCMClogf, 'mcmcStar', 'INFO', 'Found a starting file:', obsdata.startfile)
              self.init_mod=self.startfile_start(obsdata, parms)                           
           else: 
               print('Attempting a cold start')
               writer(self.MCMClogf, 'mcmcStar', 'INFO', 'Attempting cold start')
               self.init_mod=self.cold_start(obsdata, parms)

           generate, ndim= self.get_ndim(parms)
           pos, prob, rstate= self.get_mcmc_init_state(self.init_mod,generate)     
        return (pos, prob, rstate, generate, ndim, self.init_mod)

 
 
 
    def run_mcmc(self,pos, prob, rstate, generate, ndim, init_mod, walkers, obsdata):
        """Run the MCMC code"""    
        
        
        ### Need to convert the init_mod to an array. 
        ### Probably should have read it in as one initially        
        if not self.restart:
            init_mod=self.convert_init_mod(init_mod)
            self.itr=0

        ### Initialise the sampler
        sampler = emcee.EnsembleSampler(
            walkers, ndim, self.ln_posterior, args=[obsdata], threads=walkers + 1)        
            
         
        ### It would be nice to set it off in one big loop. But we can't
        ### Our model evaluation is complicated and prone to fail.
        ### We need to break it up and save periodically 
        self.generate=generate
        self.converged=False
        num_chains=self.parms.save_chain_len   

        while not self.converged:

           if self.parms.verbose: 
             print('pos', pos)
             print('prob',prob)
             print('rstate',rstate)
             print('generate',generate)
             print('ndim',ndim)
             print('init_mod',init_mod)
             print('walkers', walkers)
             print('num_chains',num_chains)   
         
           self.itr +=1 
           sampler.reset()  

           ### Calculate the chains 
           print("Running MCMC Iteration= ", self.itr)
           pos, prob, rstate = sampler.run_mcmc(
                pos, num_chains, lnprob0=prob, rstate0=rstate)

           #print('prob',prob.shape)
           #print('pos', pos.shape) 

           ### Save and plot Progress
           self.save_chains(self.MCMClogdir,generate, init_mod, sampler)
           self.plot_chains(self.MCMClogdir, generate, ndim, self.chains)  
           print('Iteration ', str(self.itr), 'saved. Computed', self.chains.shape[1])
          
           ###Check concergence           
           self.converged=self.check_convergence(sampler.chain) 
           
    def ln_priors(self, theta):
         """I am of the persuassian that we should return uniformative priors"""
         if self.parms.verbose:
             for i, col in enumerate(self.generate):
                 print(self.lims[col][0], self.lims[col][1], theta[i])
         
         if  all( [self.lims[col][0] < theta[i]   < self.lims[col][1] for i,col in enumerate(self.generate)] ) :
              return 0.0
         else:
             return -np.inf


    def ln_posterior(self, theta, obsdata):
        lnp = self.ln_priors(theta)
        # print(lnp)
        if not np.isfinite(lnp):
            return -np.inf
        # Write to Disk here
        return lnp + self.ln_likelihood(theta, obsdata)


    def ln_likelihood(self,theta, obsdata): 
        return self.eval_stellar_model(theta, obsdata)


    def eval_stellar_model(self, theta, obsdata):
        """Calculate a stellar model and return the log ln_likelihood for the fit"""
        
        ### Should be and if gars statement up here. Its pretty gars specific

        ###An array of parameters to pass to the stellar code. Set age separately 
        arr = np.zeros(1, dtype=([('ID', (str, 80)), ('M', 'f8'), ('X', 'f8'), (
            'Z', 'f8'), ('Y', 'f8'), ('alpha', 'f8'),('overshoot', 'f8'), ('C', 'f8'), ('N', 'f8'), ('O', 'f8')]))
        
        ### Fill the array with the parameters we margainilise over 
        for i,key in enumerate(self.generate):
           if key in arr.dtype.names: 
              arr[key]=theta[i]  
           if key=='age':
              AGEend=float(theta[i])
           if key=='overshoot':
              OS= float(theta[i])
              
        if self.parms.verbose:
          print('eval_stellar_model', arr)     
         
         
        ### Now special cases of Z
        if self.parms.z_free == 0:
            arr['Y'] = 1.0 - arr['X'] - arr['Z']
        elif self.parms.z_free == 1:
           arr['Z'] = self.parms.zx * arr['X']
           arr['Y'] = 1.0 - arr['X'] - arr['Z']
        elif self.parms.z_free == 2: 
            arr['Y'] = self.parms.y0_fixed
            arr['X'] = 1.0 - arr['Z'] - arr['Y']        
        elif self.parms.z_free == 3:
            arr['Y'] = self.parms.y0_fixed + self.parms.dydz * arr['Z']
            arr['X'] = 1.0 - arr['Z'] - arr['Y']

        ### We don't change CNO at the moment
        if 'C' in self.init_mod.dtype.names:
           arr['C'] = self.init_mod['C']
           arr['N'] = self.init_mod['N']
           arr['O'] = self.init_mod['O']
        else:
           arr['C'] = -1.7373e-3
           arr['N'] = -1.7373e-3
           arr['O'] = -1.7373e-3
           
        ### Lets not forget old mate alpha_mlt
        if 'alpha' not in self.generate:
            arr['alpha']=self.parms.alpha_mlt

        if 'overshoot' not in self.generate:
            arr['overshoot']=self.parms.overshoot


        ### Set up run directories.   
        starfolders = [self.make_starfolder(arr, AGEend)]

        ### For the MCMC the ID will be the dirname
        arr['ID']=starfolders[0][1]  
        
        
        ### Set up dictionary with parms/keys to pass to evcode
        mcmcp = {'AgeEnd': AGEend, 'GongLast': 1}
        
        if not math.isclose(arr['overshoot'], 0.0, rel_tol=1e-5):            
           mcmcp['EffMix']=OS
       
        ### Garstec models dont always run. 
        ### Lets loop this trying with different starting models
        ### Hopefully one of them works
        self.evparms.evfunc.copy_runfiles(starfolders, self.evparms.evparm)
        if self.parms.evcode=='GARS':
           ### We now have an iterator of models to try" 

           self.initmod_gen=iter(self.evparms.evfunc.get_initial_model_list(arr, starfolders))
           cutfail=True
           
           fail_count=0
           while True: 
               
              ### First step is to get a new model that was succesfully cut 
              while cutfail is True:
                 cutfail=self.evparms.evfunc.get_initial_model(arr, starfolders[0], idx=next(self.initmod_gen)) 

              if fail_count > 0: 
                mod=starfolders[0][1]+ '.mod'
                if self.parms.verbose:
                   print(mod)
                   print('files pre clean', os.listdir(starfolders[0][0]))

                ### Do this in a couple of steps so its clear
                delfiles=[os.path.join(starfolders[0][0],x) for x in os.listdir(starfolders[0][0]) if x not in ['Abundances.dat', self.evparms.evparm.exe, mod]]
                delfiles=[x for x in delfiles if os.path.isfile(x)]                      
                [os.remove(x) for x in delfiles] 

                ### Better to check than ignore errors.  
                gongdir=os.path.join(starfolders[0][0], 'GONGS')
                amdldir=os.path.join(starfolders[0][0], 'AMDL')               
                if os.path.isdir(gongdir):
                   shutil.rmtree(gongdir) 
                if os.path.isdir(amdldir):   
                   shutil.rmtree(amdldir)      
                if self.parms.verbose:  
                   print('files post clean', os.listdir(starfolders[0][0]))

              run = structure(0,
                              arr[0],
                              starfolders[0],
                              self.parms,
                              self.evparms,
                              self.obsdata,
                              self.rellogf,
                              [],
                              monitor=False,
                              mcmc_parms=mcmcp)
         
         
         
              ### I am returning the negative of the log-likelihood from calc_model but the code actually diverges. 
              ### -log likelihood is what you want to minimize in a typical optimization objective function
              ### However the walkers diverge from the minima. So perhaps returning the negative is wrong. 
              ### Multiply by negative one here so its positive again 
              lnlh = run.output['mle_total']*-1.0
              
              
              if self.parms.verbose:
                  print(arr['ID'][0], 'Chi, MLE=', run.output['mle_total'], run.output['chi_total']) 
              
              if not np.isfinite(lnlh):
                 ### I think we have failed try with another model
                 cutfail = True
                 fail_count +=1
                 print(arr['ID'][0], 'fail count = ',fail_count)
                 if fail_count ==50:
                     print(arr['ID'][0],'LOL, 50 models have failed. really??') 
                     return lnlh
              else:
                 return lnlh
                

        else: 
           ### For a better behaved code   
           self.evparms.evfunc.get_inital_model(arr, starfolders)
           ### Compute and evalutate  
           run = structure(
              0,
              arr[0],
              starfolders[0],
              self.parms,
              self.evparms,
              self.obsdata,
              self.rellogf,
              [],
              monitor=False,
              mcmc_parms=mcmcp)
         


           lnlh = run.output['mle_total']
           os.chdir(self.parms.home)
           return lnlh
    
    
    def print_vars(self):
           print('self.obsdata.name=',self.obsdata.name)
           print('self.obsdata.obsfile=',self.obsdata.obsfile)
           print('self.obsdata.root=',self.obsdata.root)
           print('self.run_out=',self.run_out)
           print('self.star_out=',self.star_out)
           print('self.MCMCdir=',self.MCMCdir)
           print('self.MCMClogdir=', self.MCMClogdir)
           print('self.MCMClogf=',self.MCMClogf)
           
           
    def set_parm_limits(self,parms):
        """Set Parameter Limits"""
                        
        M_max = parms.mrange[1] if parms.mrange[1] > 0 else 1.8
        M_min = parms.mrange[0] if parms.mrange[0] > 0 else 0.8
        H_max = parms.hrange[1] if parms.hrange[1] > 0 else 0.78
        H_min = parms.hrange[0] if parms.hrange[0] > 0 else 0.60
        Z_max = parms.zrange[1] if parms.zrange[1] > 0 else 0.04
        Z_min = parms.zrange[0] if parms.zrange[0] > 0 else 1e-3
        Y_max = parms.yrange[1] if parms.yrange[1] > 0 else 0.35
        Y_min = parms.yrange[0] if parms.yrange[0] > 0 else 0.23
        omin  = parms.orange[0] if parms.orange[0] > 0 else 0.0
        omax  = parms.orange[1] if parms.orange[1] > 0 else 0.8
        A_max = 2.5
        A_min = 1.6
        age_min = 50
        age_max = 14000
        
        self.lims={}
        self.lims['M']=(M_min, M_max)
        self.lims['X']=(H_min,H_max)
        self.lims['Y']=(Y_min,Y_max)
        self.lims['Z']=(Z_min, Z_max)
        self.lims['alpha']=(A_min,A_max)
        self.lims['overshoot']=(omin,omax) 
        self.lims['age']=(age_min,age_max)
        

    def startfile_start(self, obsdata, parms):
        """Initialise from the provided starting file"""
        model={}
        if parms.starting_pattern=='_sk.dat':
           data=pd.read_table(obsdata.startfile, delim_whitespace=True)

           ### Get what we need from the startfile, otherwise we have to guess 
           for key in self.model.keys():
              if key in list(data): 
                model[key]=data[key].mean()
                model[key+'_ERR']=data[key].std()
                if key=='age':
                   ### RF gives age in Gyr not Myr  
                   model[key]=model[key]*1000
                   model[key+'_ERR']=model[key+'_ERR']*1000
              else: 
                 model[key], model[key+'_ERR']=self.mcparm_eval(key,obsdata)
                 
           ### I think I need X in here      
           if 'X' not in self.model.keys():
               model['X']=1.0-model['Y']-model['Z']
               model['X_ERR']=model['X']*0.02
               
        return model 


    def cold_start(self,obsdata, parms):
        """Intialise from a cold start. """
        model={}
        for key in self.model.keys():
          model[key], model[key+'_ERR']=self.mcparm_eval(key,obsdata)
        
        if parms.verbose:
            print(model)
        return model


    def npz_start(self, npzf):
        """Simply load everything from the chainfile"""
        ### We convert probs to a list because of issues with 0D and boradcasting. Ugggg
        arr = np.load(npzf)
        self.chains=arr['chain']
        self.probs=arr['prob']
        rstate = np.random.get_state()
        generate= arr['generate']
        init_mod = arr['init_mod']
        self.itr=int(arr['itr'])

        ### Need the last restart iteration. We save everything
        pos=np.array(self.chains[:, self.parms.iter_restart, :])
        probs=self.probs[:,self.parms.iter_restart]  

        return (pos, probs, rstate, generate, init_mod)
 
    def get_ndim(self, parms):
        """Work out how many dimensions we have for our walkers"""
         
        ###Always want M, Age 
        generate=['M']

        ###Alpha    
        if parms.alpha_mlt < 0:         
           generate.append('alpha')

        ###Overshoot
        if parms.overshoot < -1e-5:
            generate.append('overshoot')
        
        ### Z_free =0 ---> gererate X,Z 
        if parms.z_free==0:
          generate.append('X')
          generate.append('Z')

        ###Z_free =1 ---> We fix Z/X to the solar value 
        elif parms.z_free==1:
          generate.append('X')

        ###Z_free =2 ---> Y is fixed (Y0), ZX can vary 
        elif parms.z_free==2:         
          generate.append('X')
          generate.append('Z')

        ###Z_free =2 ---> Y comes from dydz relation, Z is free, X is conserved    
        elif  parms.z_free==3:       
           generate.append('Z')

        ### Always want age last 
        generate.append('age')
        
        return generate, len(generate)


    def mcparm_eval(self,key, obsdata):
        """Come back with a guess for the parameter"""
        model={}

        try:
           xcols=list(obsdata.obs['name']) 
           inmx=xcols.index('nu_max')
           jnmx=xcols.index('Teff')
 
           Dnu0=obsdata.freqs.freq_consts['Dnu0'] 
           Teff=obsdata.obs['value'][jnmx]
           nmx=obsdata.obs['value'][inmx]
            
           model['M']= (nmx / self.parms.nmsun)**3 * (Dnu0/ self.parms.dnusun)**(-4) * (Teff / self.parms.tsun)**1.5

        except:
           model['M']=1.0    

        ### Anything else we want to try???
        model['age'] = 5000.0
        model['X'] = 0.7019
        model['Y'] = 0.2809
        model['Z'] = 0.0172
        model['C'] = -1.737e-3
        model['N'] = -1.737e-3
        model['O'] = -1.7373e-3
        if self.parms.alpha_mlt < 0:
           model['alpha'] = 1.737
        else: 
           model['alpha']= self.parms.alpha_mlt

        if self.parms.alpha_mlt < -1e5:
            model['overshoot']= 0.3
        return model[key], 0.25*model[key]


    def get_mcmc_init_state(self, model, generate) :
       """Get the things we need to start the MCMC algorithm"""
       start_prob = None
       rstate = np.random.get_state()
       p0=[ [np.random.normal(model[key],model[key+'_ERR']) for key in generate] for i in range(self.nwalkers)] 
       p0=np.array(p0)
       return p0, start_prob, rstate




    def save_chains(self, MCMClogdir,generate, init_mod, sampler):
       """Save the current progress of the chain. The format of the npz file will be
          MCMCdir,chain, prob, state, generate, init_mod """
       if self.itr > 1:
          self.chains=np.concatenate((self.chains, sampler.chain), axis=1) 
          self.probs = np.concatenate((self.probs, sampler.lnprobability), axis=1)

       else:
          self.chains= sampler.chain
          self.probs = sampler.lnprobability                   
       
       ### Save the backup to the logdir
       ### I dont think there is any need to save the state. More effort than its worth.
       ### And only useful for trying to reproduce exact behaviour which is rarely needed.   
       savef=os.path.join(MCMClogdir,self.obsdata.name+ '_chain.npz')
       np.savez(savef,
                chain=self.chains,
                prob=self.probs,
                generate=generate,
                init_mod=init_mod,
                itr=self.itr)

    def plot_chains(self, MCMClogdir, generate, ndim, chains):
       y_latex, y_latex_short=ylabels() 
       fig, axes = pl.subplots(ndim, 1)
       labels=[y_latex[key] for key in generate] 
       for i in range(ndim):
          axes[i].plot(chains[:, :, i].T, color="k", alpha=0.4)
          axes[i].yaxis.set_major_locator(MaxNLocator(5))
          axes[i].set_ylabel(labels[i])

       fig.tight_layout(h_pad=0.0)
       fig.savefig(os.path.join(MCMClogdir, 'walkers_parm.pdf'))
         
       if chains.shape[1] >  self.parms.burn_in+ndim:
          samples = chains[:, self.parms.burn_in:, :].reshape((-1, ndim))
       else: 
          samples = chains.reshape((-1, ndim))
       #fig = corner.corner(samples, labels=labels, truths=truevs)
       fig = corner.corner(samples, labels=labels)

       fig.savefig(os.path.join(MCMClogdir, "model_parms.pdf"))  



    def check_convergence(self, chains):
       """Calculate Rhat (Gelman and Rubin 1992, Statistical Science, 7, 457) 
       for a given set of chains. """
       
       n = chains.shape[1]  # number of iterations
       m = chains.shape[0]  # number of chains (walkers)
       global_mean = np.mean(chains)
       chains_mean = np.mean(chains, 1)
       Bn = np.sum((chains_mean - global_mean)**2) / (m-1)
       W = np.sum([np.sum([(chains[j,t] - chains_mean[j])**2
                        for t in range(n)])
                for j in range(m)]) / (m * (n-1))
       Rhat = ((m+1) * (n-1) / (m*n))  +  ((m+1) * Bn / (m*W))  -  ((n-1)/(m*n))
       if Rhat > 1:
        return False
       else: 
        return True
    
    
    def make_starfolder(self, genes, age):
        """Create the Individual folders for each star"""

        if self.parms.verbose:
            print(genes, age)
            
        rrundir = os.path.join(self.MCMCdir, 'Iter' + str(self.itr))
        dirname = """M{}_X{}_Y{}_Z{}_A{}_O{}_AGE{}""".format(
                               np.round(genes['M'], 5)[0], 
                               np.round(genes['X'], 5)[0], 
                               np.round(genes['Y'], 5)[0], 
                               np.round(genes['Z'], 5)[0], 
                               np.round(genes['alpha'], 5)[0],
                               np.round(genes['overshoot'], 5)[0], 

                               np.round(age, 3))
        
        starfolder = os.path.join(rrundir, dirname)

        while os.path.isdir(starfolder):
            i = 0
            dirname = """{}_M{}_X{}_Y{}_Z{}_A{}_O{}_AGE{}""".format(
                              'V' + str(i), 
                               np.round(genes['M'], 5)[0], 
                               np.round(genes['X'], 5)[0], 
                               np.round(genes['Y'], 5)[0], 
                               np.round(genes['Z'], 5)[0], 
                               np.round(genes['alpha'], 5)[0], 
                               np.round(genes['overshoot'], 5)[0], 
                               round(age, 3))
            starfolder = os.path.join(rrundir, dirname)

        print(starfolder)
        os.makedirs(starfolder)

        return (starfolder, dirname)
    

    def convert_init_mod(self,init_mod):
         """Converts init_mod dictionary to structured array"""
         
         ### ensure the same ordering  
         keys=list(init_mod.keys())   
         dtypes=[(key, float) for key in keys]
         data=[init_mod[key] for key in keys]
         init_mod=np.array(tuple(data), dtype=dtypes)
         self.init_mod=init_mod
         return init_mod
     

     
