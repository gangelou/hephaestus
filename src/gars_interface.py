import os
import subprocess as sp
import numpy as np
import math
from src.ggutils import writer, InitialModels
import shutil
import re
import time
import sys

class garstec(object):
    """Library of functions specifically for Garstec Stellar Evolution Code"""

    def getzx(self, parms):
        """Method to determine the value of Z/X for the respective Evolution code"""
        with open(parms.garsdir + '/Abundances.dat', 'r') as af:
            zx = af.readline()
            return float(zx)

    def readlog(self, fname):
        """Method to read the Gars evolutionary log file. 
           Race condition stuff introduced"""
        i=0
        while i <= 20:
           if not os.path.isfile(fname) or os.stat(fname).st_size < 500:
              time.sleep(6)
              i+=1
           else: break
  
        dt=0
        bc=0
        while dt < 6:            
           file_mod_time = os.stat(fname).st_mtime
           now_time = time.time() 
           dt = now_time-file_mod_time  
           time.sleep(6)
           bc+=1 
           if bc >=10: break

        return np.genfromtxt(
            fname,
            comments='#',
            delimiter=[
                7,
                16,
                7,
                3,
                6,
                7,
                9,
                8,
                6,
                7,
                7,
                6,
                7,
                5,
                8,
                7,
                7,
                7,
                5,
                4,
                8,
                8,
                8,
                4])

    def get_Z(self, fname=None, arr=None):
        """Method to determine surface Z inthe respective Evolution code. 
           Use log3 here different to other parameters. Always use load the log """
           
        w3 = np.genfromtxt(
            log3, comments='#', delimiter=[
                7, 8, 8, 5, 4, 3, 9, 9])
        w3 = np.insert(w3, 0, w3[0], axis=0)
        return [1 - w3[:, 7][kk] - w3[:, 6][kk] for kk in range(len(w3))]

    def get_aMLT(self, gparms):
        """Method to determine the value of alpha MLT in respective Evolution code param file"""
        if self.name.lower() == 'gars':
            Alpha_MLT = gparms.CONVTYPE['ALPHA']
            ind = str.find(Alpha_MLT, '@')
            return float(Alpha_MLT[:ind])


    def get_initial_model_list(self, arr, dest_folder, parms):    
         return InitialModels.get_startmod(self, [arr['M'][0], arr['Z'][0]], parms.init_modlist)
        
    def get_initial_model(self, arr, dest_folders, parms,idx=0): 
      starfolder=dest_folders[0]
      dirname=dest_folders[1]

      fdx = os.path.join(starfolder, dirname + '.mod')
      if os.path.isfile(fdx):
          os.remove(fdx)
      if parms.verbose:  
         print(starfolder, dirname) 
         print('idx=',idx)      
         print( parms.init_modlist['fname'][idx])
         print( starfolder + '/' + dirname + '.mod')
         print( str(int(parms.init_modlist['NMOD'][idx])))
      sp.call(["cutmod",
                     parms.init_modlist['fname'][idx],
                     starfolder + '/' + dirname + '.mod',
                     str(int(parms.init_modlist['NMOD'][idx]))],
                    stdout=sp.DEVNULL,
                    stderr=sp.DEVNULL)

      
      if os.path.getsize(os.path.abspath(fdx)) == 0:
        return True
      else: 
          return False
        
    def get_inital_model_old(self, arr, dest_folders, parms):
        """Method to find and extract the starting model for the respective Ev code"""
        starfolder = [x[0] for x in dest_folders]
        dirname = [x[1] for x in dest_folders]

        for i in range(len(starfolder)):
            idx = InitialModels.get_startmod(
                self, [arr['M'][i], arr['Z'][i]], parms.init_modlist)
            sp.call(["cutmod",
                     parms.init_modlist['fname'][idx],
                     starfolder[i] + '/' + dirname[i] + '.mod',
                     str(int(parms.init_modlist['NMOD'][idx]))],
                    stdout=sp.DEVNULL,
                    stderr=sp.DEVNULL)

            fdx = os.path.join(starfolder[i], dirname[i] + '.mod')
            while os.path.getsize(os.path.abspath(fdx)) == 0:
                os.remove(fdx)
                idx += 1
                sp.call(["cutmod",
                         parms.init_modlist['fname'][idx],
                         starfolder[i] + '/' + dirname[i] + '.mod',
                         str(int(parms.init_modlist['NMOD'][idx]))],
                        stdout=sp.DEVNULL,
                        stderr=sp.DEVNULL)





    def copy_runfiles(self, dest_folders, evparms, parms):
        """Method to copy the run files to required directories"""
        starfolders = [x[0] for x in dest_folders]
        dirname = [x[1] for x in dest_folders]

        for f in os.listdir(parms.garsdir):
            if f == evparms.name:
                pass
            else:
                f = os.path.join(parms.garsdir, f)
                [shutil.copy(f, source) for source in starfolders]

    def get_Teff(self, fname=None, arr=None):
        """Method to extract temp from log file"""
        if arr is not None:
            w = arr
        else:
            w = self.readlog(fname) 
        return [10**w[:, 10][i] for i in range(len(w))]

    def get_FeH(self, zx, fname=None):
        """Method to extraxt [Fe/H] from Garstec Output """
        w = self.readlog(fname)
            
        ### Hacky and quick. No Alpha enhancement. Fix
        log3 = fname + '3'
        w3 = np.genfromtxt(
            log3, comments='#', delimiter=[
                7, 8, 8, 5, 4, 3, 9, 9])
        w3 = np.insert(w3, 0, w3[0], axis=0)
        wz = [1 - w3[:, 7][kk] - w3[:, 6][kk] for kk in range(len(w))]
        feh = [math.log10(wz[kk] / w3[:, 6][kk]) - math.log10(zx)
               for kk in range(len(w))]
        return feh

    def get_log_g(self, fname=None, arr=None):
        """Method to calculate Log G from garstec output"""
        if arr is not None:
            w=arr
        else:
           w = self.readlog(fname) 
        
        logg = [(-10.61083 + math.log10(w[:, 4][i]) + 4.0 *
                 (w[:, 10][i]) - (w[:, 6][i])) for i in range(len(w))]
        return logg

    def get_mass(self, fname=None, arr=None):
        """Method to return star mass from garstec output"""
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname)           
        return w[:, 4]

    def get_lum(self, fname=None, arr=None):
        """Method to return luminosity from garstec output"""
        if arr is not None:
            w=arr
        else:
           w = self.readlog(fname)

        return [10**w[:, 6][i] for i in range(len(w))]

    def get_nu_max(self,NMsun, TeffSun, fname=None, arr=None):
        """Method to calculate Log G from garstec output"""
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname) 
        
        nmx = [(NMsun * w[:, 4][i] * ((10.0**w[:, 10][i]) / TeffSun)
                ** 3.5) / 10.0**w[:, 6][i] for i in range(len(w))]
        return nmx

    def get_mods(self, fname=None, arr=None):
        """Return list of models from garstec logfile"""
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname) 
        return w[:, 0]

    def get_Xcentre(self, fname=None, arr=None):
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname) 
        xc = w[:, 14]
        xc = [0.0 if x > 0.0 else -x for x in xc]
        return xc

    def get_AGE(self, fname=None, arr=None):
        if arr is not None:
           w = arr
        else:
           w = self.readlog(fname) 
        return w[:, 1]

    def archlog(self, logf, zxsun):
        """Get Essential info for archiving from garstec"""
        log7 = logf + '7'
        if os.path.isfile(log7):

            with open(f, 'r') as f:
                head = [next(f) for x in xrange(4)]

            ver = float(head[-1].split(':')[-1].strip())
            if ver == 0.1:
                w = np.genfromtxt(
                    f,
                    skip_header=3,
                    names=True,
                    delimiter=',',
                    dtype=None)

        else:
            w = self.readlog(logf)
            ver = 0.0
            feh = self.get_FeH(logf, zxsun)
            w = np.hstack((w, np.reshape(feh, (-1, 1))))
        return w, ver

    def runev(self, arr, obsdata, parms, evparms, warnf, monitor):
        """Run and track Gars output"""
        cmd = './' + evparms.evparm.exe
        arg = evparms.evparm.pname
        
        if parms.verbose:
            print('Executing Gars for ' + str(arr['ID']))
        
        writer(warnf, 'RunGars run', 'INFO',
               'Executing Gars for ' + str(arr['ID']))

        if not monitor:
            ### sp.run should be thread blocking
            with open('stdout.txt', 'a') as tempf:
                child = sp.run([cmd, arg], stdout=tempf, stderr=tempf)
                return 1

        else:
            with open('stdout.txt', 'a') as tempf:
                child = sp.Popen([cmd, arg], stdout=tempf, stderr=tempf)

            exitcode = 0
            logf = arr['ID'] + '.log'
            # I Dont know why call or sp.wait() doesnt work.
            while True:
                if child.poll() is not None:
                    break
                else:
                    time.sleep(60)
                    if os.path.isfile(logf):
                        w = self.readlog(logf)
                        exitcode = self.interrupt(
                            w, parms, warnf, arr, obsdata)
                        if exitcode == 1:
                            break
                    else:
                        pass
            # if exitcode ==0: #XXX Need something if it fails
            return exitcode

    def interrupt(self, w, parms, warnf, arr, obsdata, live=0):
        """Evolved Far Enough.
          'DNU' - Evolve until Asymp Delta nu is too small
          'NM'  - Evolve until Asymp Nu Max is too small
          'MS' - Use abunds to guess end of MS
          'MOD' - Evolve to this model number (Give stop value)
          'AGE' - Evolve to this Age (Give stop value in Myr)
          'NO' - Evolve as far as possible

          Nu-Max - Delta nu relation taken from stello et al. 2009
          Dnu=2.63*numax^0.772

          Normally Return 1 is success"""

        # Quick test because of write out race condition
        try:
            (parms.nmsun * w[:, 4][-1] * ((10.0**w[:, 10]
                                           [-1]) / parms.tsun)**3.5) / 10.0**w[:, 6][-1]
        except IndexError:
            return 0

        stop = False
        for ii in range(len(parms.stop_criteria)):

            sc = str(parms.stop_criteria[ii])
            sv = float(parms.stop_value[ii])
            eps = float(parms.stop_eps[ii])

            if sc == 'DNU':
                nmx = (parms.nmsun * w[:, 4][-1] * ((10.0**w[:, 10]
                                                     [-1]) / parms.tsun)**3.5) / 10.0**w[:, 6][-1]
                dnu = 0.263 * nmx**0.772
                # Adding and multiplying is correct here. The eps is added to
                # parameter not the stopping condition value
                if eps >= 0:
                    testval = dnu + eps
                else:
                    testval = dnu * abs(eps)

                if len(obsdata[parms.catdict['DNU']]) > 0:
                    sc = obsdata[parms.catdict['DNU']]

                if testval < sc:
                    stop = True
                    writer(warnf,
                           'interrupt',
                           'INFO',
                           str(arr['ID']) + ' Evoln stopped because DNU condition',
                           'dnu,eps=' + str(dnu) + ' ' + str(eps))

                # print(nmx,dnu,self.obsdata[parms.catdict['DNU']],stop)
            elif sc == 'NM':
                nmx = (parms.nmsun * w[:, 4][-1] * ((10.0**w[:, 10]
                                                     [-1]) / parms.tsun)**3.5) / 10.0**w[:, 6][-1]

                if eps >= 0:
                    testval = nmx + eps
                else:
                    testval = nmx * abs(eps)

                if len(obsdata[parms.catdict['NU_MAX']]) > 0:
                    sc = obsdata[parms.catdict['NU_MAX']]
                if testval < sc:
                    stop = True
                    writer(
                        warnf, 'interrupt', 'INFO', str(
                            arr['ID']) + ' Evoln stopped because Nu Max condition')

            elif sc == 'MS':

                # for i in range(len(w)):
                    # Using abundances not so robust. Will Try log g instead. Actually when core abundance less than 1e3. H abunds neg
                    # if int(w[:,17][i]*1000) > int(w[:,17][i-1]*1000):
                    # if -10.61083+math.log10(w[:,4][i])
                    # +4.0*(w[:,10][i])-(w[:,6][i]) <=3.0:
                if w[:, 14][-1] > -1.0e-3:
                    stop = True
                    writer(
                        warnf, 'interrupt', 'INFO', str(
                            arr['ID']) + ' Evoln stopped because reached end of Main Sequence')
            elif sc == 'MOD':
                if int(w[:, 0][-1]) >= int(sv):
                    stop = True
                    writer(
                        warnf, 'interrupt', 'INFO', str(
                            arr['ID']) + ' Evoln stopped because reached desired model')
            elif sc == 'AGE':
                if float(w[:, 1][-1]) >= float(sv):
                    stop = True
                    writer(
                        warnf, 'interrupt', 'INFO', str(
                            arr['ID']) + ' Evoln stopped because reached desired age')

        if float(w[:, 1][-1]) >= 13900:
            stop = True
            writer(
                warnf, 'interrupt', 'INFO', str(
                    arr['ID']) + ' Evoln stopped because reached age of Universe')

        if stop:
            if live == 1:
                return 1

            with open('parmeter.int', 'w') as f:
                f.write('I')
                return 1

        else:
            return 0


class gars_parmfile(object):

    def __init__(self, garsdir, lf='./log.xxx', **kwargs):
        self.garsdir = garsdir
        self.lf = lf
        self.parmf = self.get_parm(self.garsdir)
        self.defpm = self.default_parm(self.parmf)

        self.ftu = ""
        self.hinfo = ""
        self.sysv = ""

    def __call__(self, att, **kwargs):

        if att == 'ftu':
            self.ftu = self.files_to_use(**kwargs)
        elif att == 'hinfo':
            self.hinfo = self.header_info(**kwargs)
        elif att == 'sysv':
            self.sysva = self.sysv_args(**kwargs)
        # print(self.ftu)
        # print(self.hinfo)

    def get_parm(self, garsdir):
        parmf = ''
        for f in os.listdir(garsdir):
            size = os.path.getsize(os.path.join(garsdir, f))
            if size < 50000:
                if '~' not in f:
                    try:
                        with open(os.path.join(garsdir, f), 'r') as pf:
                            l1 = pf.readline()
                            if 'FILES TO BE USED' in l1:
                                parmf = os.path.join(garsdir, f)
                                print(
                                    'Assuming', parmf, ' is the default parm file')
                                writer(
                                    self.lf,
                                    'get_parm',
                                    'INFO',
                                    'Assuming ' +
                                    str(parmf) +
                                    ' is the default parm file')
                                self.name = f
                    except:
                        print('Trouble with file', f, '--Skipping.')
                        writer(
                            self.lf,
                            'get_parm',
                            'WARN',
                            'Trouble with file' +
                            str(f) +
                            '--Skipping.')
                        pass
            else:
                print('Assuming', f, ' is the gars exe file')
                writer(
                    self.lf,
                    'get_parm',
                    'INFO',
                    'Assuming ' +
                    str(f) +
                    ' is the gars exe file')
                self.exe = f

        if len(parmf) == 0:
            print("""Unable to find default param file.
Please check garsdir variable in parameter file.
Or that a parameter file is in the (set) gars directory""")
            writer(self.lf, 'get_parm', 'ERR ', """Unable to find default param file.
Please check garsdir variable in parameter file.
Or that a parameter file is in the (set) gars directory""")
            sys.exit(1)
        else:
            return parmf

    def default_parm(self, parmf):
        """ Get the params from default parm file and put into dictionaries"""
        with open(parmf) as f:
            sec = []
            lines = f.readlines()
            for i, line in enumerate(lines):
                if '***' in line:
                    sec.append(i)

                # Get the keys
                if line.startswith('KEYS'):
                    line = line.split(':')
                    keys = list(line[1].split())
                    self.keys = [int(x) for x in keys]

            # Output Info
            self.init = lines[sec[0]:sec[1]]

            # File Header
            self.header = lines[sec[1] + 1]

            # System Variables
            sysv = lines[sec[2] + 1:]

            # Tail of parm file
            topop = []
            for i, line in enumerate(sysv):
                if line.startswith(('$', '%', '!', '#')):
                    topop.append(i)

            topop = topop[::-1]
            self.tail = [sysv.pop(ind) for ind in topop]

            # Split the sysv arguments.
            varlines = []
            for parm in sysv:
                parm = re.split(': |,', parm)
                parm = [x.strip() for x in parm]
                varlines.append(parm)

            # Each variable group will be the attribute. And each attribute
            # will be a dict
            names = []
            for lis in varlines:
                names.append(lis[0])
            names = sorted(set(names), key=lambda x: names.index(x))
            names = [name.upper() for name in names]
            self.atts = names
            for name in names:
                setattr(self, name, {})

            # Clumsy multiple looping but im pressed for time
            for lis in varlines:
                attr = lis.pop(0)

                for var in lis:
                    var = re.split(' |=', var)
                    var = [x for x in var if x]
                    if len(var) < 2:
                        pass
                    else:
                        for i in range(0, len(var), 2):
                            getattr(
                                self, attr.upper())[
                                var[i].upper()] = var[
                                i + 1]

    # Will need to pass composition and generation info for file and header
    # names
    def files_to_use(self,
                     iopath='./',
                     outf='george',
                     logf='george',
                     mod='m1.1.mod',
                     eos='/afs/mpa/project/ml2/Hephaestus/codes/Garstec13/GarsInputs/EOS/opal/2005',
                     atm='------------',
                     opac='/afs/mpa/project/ml2/Hephaestus/codes/Garstec13/GarsInputs/Opacities/Set2_P',
                     opf='OLJFA09 *',
                     opac2='--- - - - - -'):


         
        ftu = """******  STAR2010  -  FILES TO BE USED:  ******
I/O-Path    : {}
Output files: {}-??
'log' -files: {}
'mod' - file: {}
EOS-Path    : {}
Atmos-Table : {}
Opacity path: {}
OpacityFiles: {}
Opacity 2   : {}

""".format(iopath, outf, logf, mod, eos, atm, opac, opf, opac2)
        # print(ftu)
        return ftu

    def header_info(self, header=' Evolution with GE'):

        hinfo = """******  HEADER:  ******
 {}

""".format(header)

        return hinfo

    def sysv_args(self):
        sysvt = """****** Parameter values (by name) below this line ****** \n"""
        for desc in self.atts:
            jmax = len(getattr(self, desc))

            j = 1
            i = 1
            for k, v in getattr(self, desc).items():
                if i == 1:
                    strattrs = "".join('%s%s  ' % (desc, ':'))
                    strattrs = strattrs + "".join('%s=%s,   ' % (k, v))

                elif i > 1 and i <= 4:
                    strattrs = strattrs + "".join('%s=%s,   ' % (k, v))

                if i == 4 or j == jmax:
                    strattrs = strattrs + '\n'
                    sysvt = sysvt + strattrs
                    i = 0

                j += 1
                i += 1
        # print(sysvt)
        return sysvt

    def create_ev_parm(self, arr, warnf, **kwargs):
        """Create the Parm file"""
        self.pname = 'parmeter'
        #print('create parms kwargs=',kwargs)
        self('ftu',
             iopath='./',
             outf=arr['ID'],
             logf=arr['ID'],
             mod=kwargs['mod'])

        self.MAIN['RF1'] = str(-1)
        self.MAIN['NSW1'] = str(arr['M'])
        # evparms.ZEIT['DTMAX']='20.0090909091M'
        self.CONVTYPE['ALPHA'] = str(arr['alpha']) + '@0'
        self.MAIN['ITW'] = '0'
        self.CHCHEM['XIN'] = str(arr['X'])
        self.CHCHEM['YIN'] = str(arr['Y'])

        if 'C' in arr.dtype.names:
            self.CHCHEM['CIN'] = str(arr['C'])
            self.CHCHEM['NIN'] = str(arr['N'])
            self.CHCHEM['OIN'] = str(arr['O'])
        else:
            self.CHCHEM['CIN'] = str(-2.43e-4)
            self.CHCHEM['NIN'] = str(-9.07e-5)
            self.CHCHEM['OIN'] = str(-4.02e-3)

        self.CHCHEM['MCUT'] = str(arr['M'])

        kkeys = list(kwargs['OPT'].keys())
        if 'DDNU' in kkeys:
            self.ZEIT['DDNU'] = kwargs['OPT']['DDNU']
        else:
            if 'DTEF' in kkeys:
                self.ZEIT['DTEF'] = kwargs['OPT']['DTEF']

        if 'MCMC' in kwargs.keys():
            self.MAIN['AGEEND'] = str(round(kwargs['MCMC']['AgeEnd'], 3)) + 'M'
            self.MAIN['GONGLAST'] = kwargs['MCMC']['GongLast']
            if 'EffMix' in kwargs['MCMC'].keys():
               self.DIMIBU['EffMix']=kwargs['MCMC']['EffMix']

        self('hinfo', header=kwargs['mod'])
        self('sysv')
        #if self.adipls > 0: evparms.keys.append(92)
        self.keys = self.keys + kwargs['gkeys']
        self.keys = list(set(self.keys))
        with open(self.pname, 'w') as f:
            f.writelines(self.ftu)
            f.writelines(self.hinfo)
            f.writelines('KEYS: ' + " ".join([str(x)
                                              for x in self.keys]) + '\n' + '\n')
            f.writelines(self.sysva)
            f.writelines(self.tail)
  
        writer(warnf, 'createparm', 'INFO',
               'Parm file created for: ', str(arr['ID']))
