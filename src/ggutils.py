import os
import time
import datetime 
import numpy as np
import re
import errno

class FileLockException(Exception):
    pass


class FileLock(object):
    """ A file locking mechanism that has context-manager support so
        you can use it in a with statement. This should be relatively cross
        compatible as it doesn't rely on msvcrt or fcntl for the locking.
    """

    def __init__(self, file_name, timeout=400, delay=.05):
        """ Prepare the file locker. Specify the file to lock and optionally
            the maximum timeout and the delay between each attempt to lock.
        """
        self.is_locked = False
        self.lockfile = os.path.join(os.getcwd(), "%s.lock" % file_name)

        self.file_name = file_name
        self.timeout = timeout
        self.delay = delay

    def acquire(self):
        """ Acquire the lock, if possible. If the lock is in use, it check again
            every `wait` seconds. It does this until it either gets the lock or
            exceeds `timeout` number of seconds, in which case it throws
            an exception.
        """
        start_time = time.time()
        while True:
            try:
                self.fd = os.open(
                    self.lockfile,
                    os.O_CREAT | os.O_EXCL | os.O_RDWR)
                break
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
                if (time.time() - start_time) >= self.timeout:
                    raise FileLockException("Timeout occured.")
                time.sleep(self.delay)
        self.is_locked = True

    def release(self):
        """ Get rid of the lock by deleting the lockfile.
            When working in a `with` statement, this gets automatically
            called at the end.
        """
        if self.is_locked:
            os.close(self.fd)
            os.unlink(self.lockfile)
            self.is_locked = False

    def __enter__(self):
        """ Activated when used in the with statement.
            Should automatically acquire a lock to be used in the with block.
        """
        if not self.is_locked:
            self.acquire()
        return self

    def __exit__(self, type, value, traceback):
        """ Activated at the end of the with statement.
            It automatically releases the lock if it isn't locked.
        """
        if self.is_locked:
            self.release()

    def __del__(self):
        """ Make sure that the FileLock instance doesn't leave a lockfile
            lying around.
        """
        self.release()


class writer(object):
    """Writer for the main log file"""

    def __init__(self, f, loc, level, *messages, to=400, st='', how='a'):
        if st == '':
            ts = time.time()
            st = datetime.datetime.fromtimestamp(
                ts).strftime('%Y-%m-%d_%H:%M:%S')
        with FileLock(f, timeout=to) as lock:
            lf = open(f, how)
            for message in messages:
                line = '{}{} {}{} {}'.format(
                    st, ':', level, ':', message)  # '\t','['+loc+']'+ '\n')
                line = line.ljust(100) + '[' + loc + ']' + '\n'
                lf.writelines(line)

class swriter(object):
    """Writer for the star log file"""

    def __init__(self, f, *args, datype=0, to=400, st='', how='a'):

        if st == '':
            ts = time.time()
            st = datetime.datetime.fromtimestamp(
                ts).strftime('%Y-%m-%d_%H:%M:%S')

        with FileLock(f, timeout=to) as lock:
            lf = open(f, how)
            if datype == 0:
                for arg in args:
                    for ln in arg:
                        line = '  '
                        for parm in ln:
                            try:
                                parm = round(parm, 8)
                                line = line + str(parm).ljust(18)
                            except:
                                line = line + str(parm).ljust(18)
                        lf.writelines(line + '\n')

            elif datype == 1:
                line = '# '
                for arg in args:
                    for name in arg:
                        line = line + name.ljust(18)

                lf.writelines(line + '\n')

            elif datype == 2:
                for arg in args:
                    line = '# '
                    line = line + arg
                    lf.writelines(line + '\n')

            elif datype == 3:
                for arg in args:
                    lf.writelines(arg + '\n')



class InitialModels(object):
    """Find all the initial Models. Corresponding files will be different for all evolution codes """

    def __init__(self, parms):
        from src.evcode_api import evcode
        self.moddir = parms.initial_models
        self.evcode = parms.evcode
        self.ev = evcode(parms)
        self.initmods = np.zeros((0), dtype=([('NMOD', 'i8'), ('M', 'f8'), ('X', 'f8'), ('Y', 'f8'),
                                                 ('Z', 'f8'), ('fname', (str, 100))]))

        ### Save time. numpy pickle the array of models.
        ### If we have a file newer than the npz reload.
        npzfile=os.path.join(self.moddir, 'models.npz')  
        if os.path.isfile(npzfile): 
          if  max([os.path.join(self.moddir,x) for x in os.listdir(self.moddir)], key = os.path.getctime)==npzfile:
             self.initmods=np.load(npzfile)['arr_0']
             if parms.verbose:
               print('loading intial models from pickle') 
          else:  
             if parms.verbose:
               print('found newer file than the pickle in initial models. Reloading all and saving') 
             self.getfiles() 
             np.savez(npzfile,self.initmods)       
        else: 
           if parms.verbose:
               print('No pickle in initial models. loading all and saving') 
           self.getfiles()
           np.savez(npzfile,self.initmods)       
  
      
    def getfiles(self):
        self.inmods = []
        self.inlogs = []

        if self.evcode.lower() == 'gars':

            for f in os.listdir(self.moddir):
                if f.endswith('.mod'):
                    self.inmods.append(
                        os.path.abspath(
                            os.path.join(
                                self.moddir, f)))
                elif f.endswith('.log'):
                    self.inlogs.append(
                        os.path.abspath(
                            os.path.join(
                                self.moddir, f)))

            for f in self.inlogs:
                w = self.ev.readlog(f)
                want = np.zeros((len(w)), dtype=([('NMOD', 'i8'), ('M', 'f8'), ('X', 'f8'), ('Y', 'f8'),
                                                     ('Z', 'f8'), ('fname', (str, 100))]))

                want['NMOD'] = [int(x) for x in w[:, 0]]
                want['M'] = w[:, 4]
                want['X'] = [abs(x) for x in w[:, 14]]
                want['Y'] = [abs(x) for x in w[:, 15]]
                want['Z'] = [1 - want['X'][i] - want['Y'][i]
                             for i in range(len(w))]
                want['fname'] = [f[:-3] + 'mod' for i in range(len(w))]

                # Hack. Disgusting. XXX careful here.
                if np.isclose([want['Z'][0]], [0.0]):
                    z = re.findall(r'\d+', f)
                    z = int(z[-1]) * 10**(-len(z[-1]))
                    want['Z'] = [z for i in range(len(w))]

                self.initmods = np.hstack((self.initmods, want))

    def index_min(self, values):
        return min(range(len(values)), key=values.__getitem__)

    def get_startmod(self, want, initmods):

        def chi_sq_im(obs, *args):
            """Conduct Chi Squared test for model selection"""

            omin = len(obs)

            # Check we we have same number of known values as lists of parms to
            # test
            if len(obs) != len(args):
                print('error in chi_sq_im')
                print('data to match is list of length', len(obs))
                print(len(args), 'passed for minimisation')
                sys.exit(1)

            # Check all lists of parms are the same size
            for i in range(len(args) - 1):
                if len(args[i]) != len(args[i + 1]):
                    print('error in chi_sq_im')
                    print('lists of parameters are not equal length')
                    print(i, 'th variable different length to', i + 1)
                    sys.exit(1)

            chisq = []
            lmin = len(args[0])
            for k in range(lmin):
                chi = 0
                for i in range(omin):
                    chi = chi + ((args[i][k] - obs[i])**2) / args[i][k]
                chisq.append(chi)
            return chisq

        ### This is a list of Chis based on the order of the models in the array
        chis = chi_sq_im(want, initmods['M'], initmods['Z'])
        
        #idx = InitialModels.index_min(self, chis)
        
        ### Now we return an array of indicies. 
        idxs=np.argsort(np.asarray(chis))
        
        return idxs


def ylabels():
    y_latex = {
    "M": r"Mass M$/$M$_\odot$", 
    "X": r"Initial hydrogen X$_0$",
    "Y": r"Initial helium Y$_0$", 
    "Z": r"Initial metallicity Z$_0$", 
    "alpha": r"Mixing length $\alpha_{\mathrm{MLT}}$", 
    "diffusion": r"Diffusion factor D",
    "overshoot": r"Overshoot $\alpha_{\mathrm{ov}}$", 
    "age": r"Age $\tau/$Myr", 
    "radius": r"Radius R$/$R$_\odot$", 
    "mass_X": r"Hydrogen mass X", 
    "mass_Y": r"Helium mass Y",
    "X_surf": r"Surface hydrogen X$_{\mathrm{surf}}$", 
    "Y_surf": r"Surface helium Y$_{\mathrm{surf}}$",
    "X_c": r"Core-hydrogen X$_{\mathrm{c}}$",
    "log_g": r"Surface gravity log g (cgs)", 
    "L": r"Luminosity L$/$L$_\odot$",
    "mass_cc": r"Convective-core mass M$_{\mathrm{cc}}$"
     }

    y_latex_short = {
    "M": r"M$/$M$_\odot$", 
    "X": r"X$_0$",
    "Y": r"Y$_0$", 
    "Z": r"Z$_0$", 
    "alpha": r"$\alpha_{\mathrm{MLT}}$", 
    "diffusion": r"D",
    "overshoot": r"$\alpha_{\mathrm{ov}}$",
    "age": r"$\tau/$Myr", 
    "radius": r"R$/$R$_\odot$", 
    "mass_X": r"X", 
    "mass_Y": r"Y",
    "X_surf": r"X$_{\mathrm{surf}}$", 
    "Y_surf": r"Y$_{\mathrm{surf}}$",
    "X_c": r"X$_{\mathrm{c}}$",
    "log_g": r"log g", 
    "L": r"L$/$L$_\odot$",
    "mass_cc": r"M$_{\mathrm{cc}}$"
     }
    return y_latex, y_latex_short
 
